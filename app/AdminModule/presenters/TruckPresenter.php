<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use App\AdminModule\Presenters\BasePresenter;
use Nette\Application\UI\Form;

class TruckPresenter extends BasePresenter
{
	public $truck;
	
	public function renderDefault()
	{
		$this->template->items	= $this->getItems();
	}
	
	
	
	public function actionDetail($id)
    {
		$this->truck = $this->truckRepository->find($id);
		$this->getComponent('truckForm')
			->setDefaults( Array( 
				'platenumber' => $this->truck->platenumber,
			) );
	}
	
	public function renderDetail($id)
    {
		if (!$this->truck) {
			$this->error('Položka nebyla nalezena');
		}
		$this->template->item = $this->truck;
    }
	
	
	public function renderAdd()
    {
		$this->setView('detail');
    }
	
	public function actionDelete($id)
    {
		$this->truck = $this->truckRepository->find($id);
		$this->truckRepository->delete($this->truck);
		$this->redirect('Truck:default');
	}
	
	protected function createComponentTruckForm($name)
	{
		$form = new Form();
		
		$presenter = $this;
		$form->getElementPrototype()->class('form-horizontal');
		
		$rendererConfig = Array(
			'layout' => 'horizontal',
			'labels' => true,
			'submitFullWidth' => false,
			'submitAlign' => 'center',
			'leftCol' => 3,
			'rightCol' => 9,
		);
		//$form->setRenderer( new \Nette\Forms\Rendering\BootstrapFormRenderer($rendererConfig) );
		
		$platenumber = $form->addText('platenumber', 'Registrační značka' )
			->setRequired('Registrační značka je povinná!');
		$platenumber->getControlPrototype()->class('form-control');

		$send = $form->addSubmit('send', 'Uložit');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onValidate[] = Array($this, 'validateTruckForm');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$mode='edit';
			$values = $form->getValues();
			$httpRequest = $this->getContext()->getService('httpRequest');
			if( is_null($presenter->truck) ) 
			{
				$mode='insert';
				$presenter->truck = new \Model\Entity\TruckEntity;
			}
			$presenter->truck->platenumber = $values['platenumber'];
			$presenter->truckRepository->persist($presenter->truck);
			
			$presenter->redirect('Truck:default');
		};
		return $form;
	}
	
	
	//zkontroluje jestli uz neexistuje spz
	public function validateTruckForm($form)
	{
		$values = $form->getValues();
		$existing = $this->truckRepository->getOneBy( Array('platenumber'=>$values['platenumber']) );
		
		//editace
		if( $existing AND !is_null($this->truck) AND $existing->id != $this->truck->id ) {
			$form->addError('Tato registrační značka je již vložena.');
		}
		
		//insert
		if($existing AND is_null($this->truck)) {
			$form->addError('Tato registrační značka je již vložena.');
		}
	}

	private function getItems()
	{
		return $this->truckRepository->findAll();
	}
	
	
	
}
