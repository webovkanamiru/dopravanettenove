<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use App\AdminModule\Presenters\BasePresenter;
use Nette\Application\UI\Form;

class ServicePresenter extends BasePresenter
{
	public $service;
	
	public function renderDefault()
	{
		$this->template->items	= $this->getItems();
	}
	
	
	
	public function actionDetail($id)
    {
		$this->service = $this->serviceRepository->find($id);
		$this->getComponent('serviceForm')
			->setDefaults( Array( 
				'name' => $this->service->name,
				'price' => $this->service->price,
			) );
	}
	
	public function renderDetail($id)
    {
		if (!$this->service) {
			$this->error('Položka nebyla nalezena');
		}
		$this->template->item = $this->service;
    }
	
	
	public function renderAdd()
    {
		$this->setView('detail');
    }
	
	public function actionDelete($id)
    {
		$this->service = $this->serviceRepository->find($id);
		$this->serviceRepository->delete($this->service);
		$this->redirect('Service:default');
	}
	
	protected function createComponentServiceForm($name)
	{
		$form = new Form();
		
		$presenter = $this;
		$form->getElementPrototype()->class('form-horizontal');
		
		$rendererConfig = Array(
			'layout' => 'horizontal',
			'labels' => true,
			'submitFullWidth' => false,
			'submitAlign' => 'center',
			'leftCol' => 3,
			'rightCol' => 9,
		);
		$form->setRenderer( new \Nette\Forms\Rendering\BootstrapFormRenderer($rendererConfig) );
		
		$name = $form->addText('name', 'Název' )
			->setRequired('Název je povinný!');
		$name->getControlPrototype()->class('form-control');
		
		$price = $form->addText('price', 'Cena' );
		$price->getControlPrototype()->class('form-control');

		$send = $form->addSubmit('send', 'Uložit');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$values = $form->getValues();
			$httpRequest = $this->getContext()->getService('httpRequest');
			if( is_null($presenter->service) ) 
			{
				$presenter->service = new \Model\Entity\ServiceEntity;
			}
			$presenter->service->name = $values['name'];
			$presenter->service->price = $values['price'];
			$presenter->serviceRepository->persist($presenter->service);
			
			$presenter->redirect('Service:default');
		};
		return $form;
	}
	
	


	private function getItems()
	{
		return $this->serviceRepository->findAll();
	}
	
	
	
}
