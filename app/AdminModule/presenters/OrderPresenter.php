<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use App\AdminModule\Presenters\BasePresenter;
use Nette\Application\UI\Form;
use PdfResponse\PdfResponse;

class OrderPresenter extends BasePresenter
{
	public $item;
	const DATE_FORMAT = 'd.m.Y';
	private $filters = Array('truck', 'date', 'shop');
	
	public function renderDefault()
	{
		$orders = $this->getOrders();
		
		$this->template->orders = $orders;
	}
	
	public function actionDetail($id)
    {
		$this->item = $this->orderRepository->find($id);
		
		
		
		$this->getComponent('orderForm')
			->setDefaults( Array(
				'items' => Array(
					'volume'			=> $this->item->volume,
					'weight'			=> $this->item->weight,
					'qty'				=> $this->item->qty,
					'ordernumber'		=> $this->item->ordernumber,
					'paid'				=> $this->item->paid,
				),
				'slot' => Array(
					'slotDate'			=> ( is_null($this->item->slotFrom) ) ? null : date('j.n.Y', $this->item->slotFrom->getTimestamp()),
					'slotTime'			=> $this->item->getNiceSlotTime(),
					'truck'				=> ( is_null($this->item->truck) ) ? null : $this->item->truck->id,
				),
				'pickup' => Array(
					'name'				=> $this->item->pickupName,
					'centre'			=> $this->item->pickupCentre,
					'street'			=> $this->item->pickupStreet,
					'city'				=> $this->item->pickupCity,
					'zip'				=> $this->item->pickupZip,
					'phone'				=> $this->item->pickupPhone,
					'email'				=> $this->item->pickupEmail,
					'contactPerson'		=> $this->item->pickupContactPerson,
					'pickupDate'		=> ( is_null($this->item->pickupDate) ) ? null : date('j.n.Y', $this->item->pickupDate->getTimestamp()),
					'pickupTime'		=> $this->item->getNicePickupTime(),
				),
				'destination' => Array(
					'company'			=> $this->item->destinationCompany,
					'name'				=> $this->item->destinationName,
					'street'			=> $this->item->destinationStreet,
					'city'				=> $this->item->destinationCity,
					'zip'				=> $this->item->destinationZip,
					'pickupDate'		=> ( is_null($this->item->destinationDate) ) ? null : date('j.n.Y', $this->item->destinationDate->getTimestamp()),
					'pickupTime'		=> $this->item->getNiceDestinationTime(),
				),
				'payment' => Array(
					'whoPay'			=> $this->item->whoPay,
					'paymentMethod'		=> $this->item->paymentMethod,
				),
				'service'				=> $this->item->getServiceIds()
			)
		);
		
		//print_r();
	}
	
	public function renderDetail($id)
    {
		if($this->item) $this->template->item = $this->item;
    }
	

	
	protected function createComponentOrderForm($name)
	{
		$form = new Form();
		
		$presenter = $this;
		$form->getElementPrototype()->class('form-horizontal');
		
		$rendererConfig = Array(
			'layout' => 'horizontal',
			'labels' => true,
			'submitFullWidth' => false,
			'submitAlign' => 'center',
			'leftCol' => 3,
			'rightCol' => 9,
		);
		$form->setRenderer( new \Nette\Forms\Rendering\BootstrapFormRenderer($rendererConfig) );

		//informace o zbozi
		$container = $form->addContainer('items');
		
		$volume = $container->addText('volume', 'Předpokládaný objem')
			->setOption('description', 'm3')
			->setRequired('Vyplňte předpokládaný objem')
			->addRule(Form::FLOAT, 'Předpokládaný objem musí být číselný');
		
		$weight = $container->addText('weight', 'Předpokládaná hmotnost')
			->setOption('description', 'kg')
			->addRule(Form::FILLED, 'Vyplňte předpokládanou hmotnost')
			->addRule(Form::FLOAT, 'Předpokládaná hmotnost musí být číselná');
		
		$qty = $container->addText('qty', 'Počet kusů v zásilce')
			->addRule(Form::FILLED, 'Vyplňte počet kusů v zásilce')
			->addRule(Form::FLOAT, 'Počet kusů v zásilce musí být číselný');
		
		$ordernumber = $container->addText('ordernumber', 'Číslo objednávky u dodavatele')
			->addRule(Form::FILLED, 'Vyplňte číslo objednávky u dodavatele');
		
		$paid = $container->addRadioList('paid', 'Zaplaceno', Array(0=>'ne',1=>'ano'));

		
		//informace o slotu
		$container = $form->addContainer('slot');
		
		$slotDate = $container->addText('slotDate', 'Vozidlo rezervováno na den');
		
		$slotTime = $container->addText('slotTime', 'Vozidlo rezervováno na čas');
		
		$truck = $container->addSelect('truck', 'Přiděleno vozidlo', $this->truckRepository->pairs( $this->truckRepository->findAll(), 'id', 'platenumber') )
			->setPrompt('žádné');
		
		
		$truck->addConditionOn( $form['slot']['slotDate'], Form::FILLED, true)
			 ->setRequired('Přidělte vozidlo');
		
		$slotDate->addConditionOn( $form['slot']['truck'], Form::FILLED, true)
			 ->setRequired('Zadejte datum rezervace vozidla');
		
		$slotTime->addConditionOn( $form['slot']['truck'], Form::FILLED, true)
			 ->setRequired('Zadejte čas rezervace vozidla');
		
		
		//informace o vyzvednuti
		$container = $form->addContainer('pickup');
		
		$shop = $container->addSelect('shop', 'Vyberte obchod', $this->shopRepository->pairs($this->shopRepository->findBy( Array('approved'=>\Model\Entity\ShopEntity::APPROVED_YES) ), 'id', 'name') )
			->setPrompt('manuální zadání');
		$shop->addCondition(Form::EQUAL, false)
			->toggle('container-setshop');
		
		$name = $container->addText('name', 'Název obchodu');
		$name->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Název obchodu je povinný');
		
		$centre = $container->addText('centre', 'Nákupní centrum');
		$centre->addConditionOn($form['pickup']['shop'], Form::EQUAL, false);
		
		$street = $container->addText('street', 'Ulice a č.p.');
		$street->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Ulice je povinná');
		
		$city = $container->addText('city', 'Město');
		$city->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Město je povinné');
		
		$zip = $container->addText('zip', 'PSČ', 5);
		$zip->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->addRule(Form::FILLED, 'PSČ je povinné!')
			->addRule(Form::MIN_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule(Form::MAX_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule( Array($this,'isValidZip'), 'Zadané PSČ vyzvednutí není platné');
		
		$phone = $container->addText('phone', 'Telefon');
		$phone->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Telefoní číslo je povinné!');
		$phone->setEmptyValue('+420')
			->setType('phone');
		
		$email = $container->addText('email', 'Email');
		$email->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Emailová adresa je povinná')
			->addRule(Form::EMAIL, 'Emailová adresa je povinná');
		$email->setType('mail');
		
		$contactPerson = $container->addText('contactPerson', 'Kontaktní osoba');
		$contactPerson->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->addRule(Form::FILLED, 'Kontaktní osoba je povinná');
		
		
		
		$pickupDate = $container->addText('pickupDate', 'Datum vyzvednutí')
			->setRequired('Zvolte datum vyzvednutí');
		
		$pickupTime = $container->addText('pickupTime', 'Čas vyzvednutí');
		
		
		
		//informace o vylozeni
		$container = $form->addContainer('destination');
		
		$company = $container->addText('company', 'Firma')
				->setRequired('Zvolte firmu');
		
		$name = $container->addText('name', 'Jméno a příjmení')
				->setRequired('Zvolte jméno a příjmení');
		
		$street = $container->addText('street', 'Ulice a č.p.')
			->setRequired('Ulice je povinná');
		
		$city = $container->addText('city', 'Město')
			->setRequired('Město je povinné');
		
		$zip = $container->addText('zip', 'PSČ', 5)
			->addRule(Form::FILLED, 'PSČ je povinné!')
			->addRule(Form::MIN_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule(Form::MAX_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule( Array($this,'isValidZip'), 'Zadané PSČ doručení není platné');

		$destinationDate = $container->addText('destinationDate', 'Datum doručení');
		
		$destinationTime = $container->addText('destinationTime', 'Čas doručení');
		
		
		//informace o platbe
		$container = $form->addContainer('payment');
		
		$whoPay = $container->addRadioList('whoPay', 'Plátce dopravy', Array(\Model\Entity\OrderEntity::WHOPAY_SENDER=>'objednavatel', \Model\Entity\OrderEntity::WHOPAY_RECIPIENT=>'příjemce'))
			->setRequired('Zvolte plátce dopravy');
		
		$paymentMethod = $container->addRadioList('paymentMethod', 'Způsob platby', Array( \Model\Entity\OrderEntity::PAYMENT_CASH=>'hotově', \Model\Entity\OrderEntity::PAYMENT_CARD=>'platební kartou'))
			->setRequired('Zvolte způsob platby');
		
		
		$service = $form->addCheckboxList('service', 'Služby', $this->serviceRepository->pairs( $this->serviceRepository->findAll(), 'id', 'nameprice'));
		
		$send = $form->addSubmit('send', 'Uložit');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$mode='edit';
			$values = $form->getValues();
			$eOrder = $this->item;
			$originalSlotFrom = $eOrder->slotFrom;
			
			//pickup
			preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($values['pickup']['pickupDate']), $matchesPickupDate);
			preg_match('/([0-9]{1,2}):([0-9]{1,2})/', trim($values['pickup']['pickupTime']), $matchesPickupTime);
			if( count($matchesPickupDate) == 4 AND count($matchesPickupTime) == 3 ) 
			{
				$eOrder->pickupDate = new \DateTime( $matchesPickupDate[3] .'-'. $matchesPickupDate[2] .'-'. $matchesPickupDate[1] .' '. $matchesPickupTime[1] .':'. $matchesPickupTime[2] .':00' );
			}
			else $eOrder->pickupDate		= new \DateTime;
			
			if( count($matchesPickupTime) == 3 ) $eOrder->pickupTime = $matchesPickupTime[1] .':'. $matchesPickupTime[2] .':00';
			else $eOrder->pickupTime = null;
			//exit;
			
			//destination
			preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($values['destination']['destinationDate']), $matchesDestinationDate);
			preg_match('/([0-9]{1,2}):([0-9]{1,2})/', trim($values['destination']['destinationTime']), $matchesDestinationTime);
			if( count($matchesDestinationDate) == 4 ) $eOrder->destinationDate = new \DateTime( $matchesDestinationDate[3] .'-'. $matchesDestinationDate[2] .'-'. $matchesDestinationDate[1] );
			else $eOrder->destinationDate		= new \DateTime;
			
			if( count($matchesDestinationTime) == 3 ) $eOrder->destinationTime = $matchesDestinationTime[1] .':'. $matchesDestinationTime[2] .':00';
			else $eOrder->destinationTime = null;
			
			//slot
			preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($values['slot']['slotDate']), $matchesSlotDate);
			preg_match('/([0-9]{1,2}):([0-9]{1,2})/', trim($values['slot']['slotTime']), $matchesSlotTime);
			if( count($matchesSlotDate) == 4 AND count($matchesSlotTime) == 3 ) $eOrder->slotFrom = new \DateTime( $matchesSlotDate[3] .'-'. $matchesSlotDate[2] .'-'. $matchesSlotDate[1] .' '. $matchesSlotTime[1] .':'. $matchesSlotTime[2] .':00' );
			else $eOrder->slotFrom = null;
			
			$eOrder->truck = ( is_null($values['slot']['truck']) ) ? null : $presenter->truckRepository->find( $values['slot']['truck'] );
			
			$eOrder->volume			= (float)$values['items']['volume'];
			$eOrder->weight			= (float)$values['items']['weight'];
			$eOrder->qty			= (int)$values['items']['qty'];
			$eOrder->paid			= (int)$values['items']['paid'];
			$eOrder->ordernumber	= trim($values['items']['ordernumber']);
			
			$eOrder->whoPay			= trim($values['payment']['whoPay']);
			$eOrder->paymentMethod	= trim($values['payment']['paymentMethod']);
			$eOrder->slotDuration	= $this->static->slotDuration;
			
			$eOrder->pickupName				= trim($values['pickup']['name']);
			$eOrder->pickupCentre			= trim($values['pickup']['centre']);
			$eOrder->pickupStreet			= trim($values['pickup']['street']);
			$eOrder->pickupCity				= trim($values['pickup']['city']);
			$eOrder->pickupZip				= trim($values['pickup']['zip']);
			$eOrder->pickupPhone			= trim($values['pickup']['phone']);
			$eOrder->pickupContactPerson	= trim($values['pickup']['contactPerson']);
			$eOrder->pickupEmail			= trim($values['pickup']['email']);
			
			$eOrder->destinationCompany	= trim($values['destination']['company']);
			$eOrder->destinationName	= trim($values['destination']['name']);
			$eOrder->destinationStreet	= trim($values['destination']['street']);
			$eOrder->destinationCity	= trim($values['destination']['city']);
			$eOrder->destinationZip		= trim($values['destination']['zip']);
			
			//service
			$serviceIds = Array();
			foreach($eOrder->service AS $service) $serviceIds[$service->id] = $service->id;
			if( isset($values['service']) AND count($values['service']) > 0 )
			{
				foreach ($values['service'] as $id) 
				{
					$service = $this->serviceRepository->find($id);
					if( !$eOrder->hasService($service) )
					{
						$eOrder->addToService($service);
					}
					else unset( $serviceIds[$id] );
				}
			}
			//mazani prebytecnych service
			if( count($serviceIds) > 0 )
			{
				foreach ($serviceIds as $id) 
				{
					$service = $presenter->serviceRepository->find($id);
					$eOrder->removeFromService($service);
				}
			}
			
			$presenter->orderRepository->persist($eOrder);
			if( !is_null($eOrder->slotFrom) ) $presenter->occupieddateRepository->updateOneDay( $eOrder->slotFrom, $this->static->workHoursBegin, $this->static->workHoursEnd, $this->static->slotDuration);
			if( !is_null($originalSlotFrom) ) $presenter->occupieddateRepository->updateOneDay( $originalSlotFrom, $this->static->workHoursBegin, $this->static->workHoursEnd, $this->static->slotDuration);
			
			$presenter->redirect('Homepage:default');
		};
		
		return $form;
	}
	
	
	public function isValidZip($control)
    {
        $existingZip = $this->zipRepository->getOneBy( Array('zip'=>$control->value) );
		if( $existingZip ) return true;
		else return false;
    }
	

	public function actionEditInvoice($id)
	{
		$this->item = $this->orderRepository->find($id);
		$this->template->item = $this->item;
		
		if( !is_null($this->item->invoiceNumber) )
		{
			$this->getComponent('invoiceForm')
				->setDefaults( Array(
					'invoiceNumber'			=> $this->item->invoiceNumber,
					'dueDate'				=> date('j.n.Y', $this->item->dueDate->getTimestamp() ),
					'invoiceCreatedDate'	=> date('j.n.Y', $this->item->invoiceCreatedDate->getTimestamp() )
				) );
		}
		else
		{
			$max = $this->orderRepository->getMax('invoiceNumber');
			$max++;
			$this->getComponent('invoiceForm')
				->setDefaults( Array(
					'invoiceNumber'			=> str_pad( $max, 4, "0", STR_PAD_LEFT ),
					'dueDate'				=> date('j.n.Y', time()+14*86400 ),
					'invoiceCreatedDate'	=> date('j.n.Y'),
				) );
		}
	}
	
	protected function createComponentInvoiceForm($name)
	{
		$form = new Form();
		$presenter = $this;
		$form->getElementPrototype()->class('form-horizontal');
		
		$rendererConfig = Array(
			'layout' => 'horizontal',
			'labels' => true,
			'submitFullWidth' => false,
			'submitAlign' => 'center',
			'leftCol' => 3,
			'rightCol' => 9,
		);
		$form->setRenderer( new \Nette\Forms\Rendering\BootstrapFormRenderer($rendererConfig) );
		

		$invoiceNumber = $form->addText('invoiceNumber', 'Číslo faktury')
			->addRule(Form::FILLED, 'Číslo faktury je povinné');
		$invoiceNumber->getControlPrototype()->class('form-control');
		
		$dueDate = $form->addText('dueDate', 'Datum splatnosti')
			->setRequired('Datum splatnosti je povinné!')
			->setType('date');
		$dueDate->getControlPrototype()->class('form-control');
		
		$invoiceCreatedDate = $form->addText('invoiceCreatedDate', 'Datum vytvoření faktury')
			->setRequired('Datum vytvoření faktury je povinné!')
			->setType('date');
		$invoiceCreatedDate->getControlPrototype()->class('form-control');
		
		$send = $form->addSubmit('send', 'Vystavit fakturu');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$values = $form->getValues();
			
			$dueDate = \DateTime::createFromFormat(self::DATE_FORMAT, $values['dueDate']);
			$invoiceCreatedDate = \DateTime::createFromFormat(self::DATE_FORMAT, $values['invoiceCreatedDate']);
			if( !empty($values['invoiceNumber']) AND $dueDate AND $invoiceCreatedDate )
			{
				//aktualizovat `order`
				$this->item->invoicePrefix			= date('y');
				$this->item->invoiceNumber			= $values['invoiceNumber'];
				$this->item->dueDate				= ($dueDate) ? $dueDate : null;
				$this->item->invoiceCreatedDate		= ($invoiceCreatedDate) ? $invoiceCreatedDate : null;;
				$this->item->lastModifiedDate		= new \DateTime;
				$this->orderRepository->persist($this->item);
			}
			$presenter->redirect('Homepage:default');
		};
		
		return $form;
	}
	
	
	/**
	 * Print PDF invoice
	 * @param int $id
	 * @throws \Netmo\Exception\InvalidStateException
	 */
	public function actionPrintInvoice($id)
	{
		$orders  = Array($this->orderRepository->find($id));
		$html = $this->generateInvoiceSource($id);
		$pdf = new PDFResponse($html);
		$pdf->documentTitle = "faktura ". $orders[0]->invoicePrefix . $orders[0]->invoiceNumber;
		
		$this->sendResponse($pdf);
	}
	
	//returns pdf
	private function generateInvoiceSource($id)
	{
		$orders  = Array($this->orderRepository->find($id));
		$httpRequest = $this->getContext()->getService('httpRequest');

		$latte = new \Latte\Engine;
		$latte->addFilter(NULL, 'App\Templating\Helpers::common');
		$html = $latte->renderToString(__DIR__ .'/../templates/Order/printInvoices.latte', Array('orders'=>$orders,'static'=>$this->static,'baseUri'=>$httpRequest->url->baseUrl) );
		return $html;
	}
	
	public function actionSendInvoice($id)
	{
		$order = $this->orderRepository->find($id);
		$httpRequest = $this->getContext()->getService('httpRequest');

		$html = $this->generateInvoiceSource($id);

		$mpdf = new \mPDF('utf-8','A4');
		//$mpdf->debug = true;
		$mpdf->WriteHTML($html);
		$mpdf->Output(__DIR__.'/../../../invoice/faktura-'. $order->invoicePrefix . $order->invoiceNumber .'.pdf', 'F');

		$this['myMail']->createMailFromStatic('invoiceSend', $order );
		$this['myMail']->addAttachment(__DIR__.'/../../../invoice/faktura-'. $order->invoicePrefix . $order->invoiceNumber .'.pdf');
		$this['myMail']->send();
		$this->flashMessage('Email odeslán', 'success');

		$order->invoiceSent = 1;
		$this->orderRepository->persist($order);
		$this->redirect('Homepage:default');
	}
	
	private function getOrders()
	{
		$vp = $this['vp'];
		
		$get = $this->request->getParameters();
		//print_r($get);
		$filterBy = Array();
		if( !$this->isAjax() AND is_array($get) )
		{
			foreach($get as $getKey => $getValue) if( in_array($getKey, $this->filters) AND !empty($getValue) ) $filterBy[ $getKey ] = $getValue;
		}
		//print_r($filterBy);
	    $paginator = $vp->getPaginator();
	    $paginator->itemsPerPage = ( count($filterBy) > 0 ) ? 1000 : $this->static->ordersPerPage;
	    $paginator->itemCount = $this->orderRepository->countOrdersBy($filterBy);
		$ids = $this->orderRepository->findOrdersBy( $filterBy, $paginator->offset, $paginator->itemsPerPage, 'id', 'DESC' );
		
		$orders = Array();
		foreach ($ids as $o => $id) 
		{
			$orders[] = $this->orderRepository->find($id); 
		}
		return $orders;
	}
	
	public function actionDelete($id)
    {
		$this->item = $this->orderRepository->find($id);
		$this->orderRepository->delete($this->item);
		$this->redirect('Order:default');
	}
	
	protected function createComponentFilterForm($name)
	{
		$form = new Form();
		$form->setMethod('get');
		$presenter = $this;

		$date = $form->addText('date', 'Datum' )
			->setAttribute('onchange', 'submit()');
		$date->getControlPrototype()->class('form-control');
		
		$truck = $form->addSelect('truck', 'Vozidlo', $this->truckRepository->pairs( $this->truckRepository->findAll(), 'id', 'platenumber') )
			->setPrompt('')
			->setAttribute('onchange', 'submit()');
		$truck->getControlPrototype()->class('form-control');
		
		$shop = $form->addSelect('shop', 'Obchod', $this->orderRepository->getShopPairs())
			->setPrompt('')
			->setAttribute('onchange', 'submit()');
		$shop->getControlPrototype()->class('form-control');
		
		$send = $form->addSubmit('send', 'Filtrovat');
		$send->getControlPrototype()->class('btn btn-md btn-primary');
		
		return $form;
	}
	
}
