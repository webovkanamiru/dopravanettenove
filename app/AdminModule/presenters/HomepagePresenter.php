<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use App\Model\Novinka;
use App\Model\Functions;

class HomepagePresenter extends BasePresenter
{

	public function renderDefault()
	{
		$this->template->calendarSummaryAssigned	= $this->orderRepository->getSummaryAssigned();
		$this->template->calendarSummaryNotAssigned = $this->orderRepository->getSummaryNotAssigned();
	}
	
	
	public function renderDaylist($year, $month, $day)
	{
		$dayList = $this->orderRepository->getDayList($year, $month, $day);
		//print_r($dayList);
		
		$trucks = $notAssigned = Array();
		if( is_array($dayList) )
		{
			foreach ($dayList as $eOrder) 
			{
				if( is_null($eOrder->truck) ) $notAssigned[] = $eOrder;
				else
				{
					if( !isset($trucks[$eOrder->truck->id]) ) $trucks[$eOrder->truck->id] = Array('data'=>$eOrder->truck, 'orders'=>Array());
					$trucks[$eOrder->truck->id]['orders'][] = $eOrder;
				}
			}
			$this->template->dayList = $trucks;
			$this->template->notAssigned = $notAssigned;
		}
		else 
		{
			$this->template->dayList = null;
			$this->template->notAssigned = null;
		}
		$this->template->year = $year;
		$this->template->month = $month;
		$this->template->day = $day;
		
		$this->template->occupied = $this->occupieddateRepository->getOneBy( Array('date'=>$year.'-'.$month.'-'.$day) );
	}
	
	
    public function renderNovinky()
    {
        $this->template->novinky = $this->novinkaService->getAll();
    }

    public function renderPridatNovinku()
    {

    }

    public function createComponentPridatNovinku()
    {
        $form = new Form();
        $form->addText('nazev', 'Název novinky');
        $form->addSubmit('btnUlozit', 'Uložit');
        $form->onSuccess[] = array($this, 'pridatNovinku');
        return $form;
    }

    public function pridatNovinku(Form $form, $values){
        $values["text"] = "";
        $n = new Novinka(null, $values["nazev"], Functions::makeUrl($values["nazev"]), date("Y-m-d"), $values["text"], 0);
        if($this->novinkaService->save($n)){
            $this->flashMessage('Novinka úspěšně přidána.', 'success');
        }else{
            $this->flashMessage('Někde došlo k chybě.', 'danger');
        }
        $this->redirect('Homepage:Novinky');

    }
}
