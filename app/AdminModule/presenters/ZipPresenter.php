<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use App\AdminModule\Presenters\BasePresenter;
use Nette\Application\UI\Form;

class ZipPresenter extends BasePresenter
{
	public $item;
	
	
	
	public function renderDefault()
	{
	}
	
	
	public function renderTable()
	{
		$offset  = (int)$this->getHttpRequest()->getQuery("iDisplayStart");
		$perPage = (int)$this->getHttpRequest()->getQuery("iDisplayLength");
		
		$cols = Array( 'id', 'zip' );
		$orderBy = $cols[ (int)$this->getHttpRequest()->getQuery("iSortCol_0") ];
		
		$filters = Array();
		foreach ($cols as $c => $col) 
		{
			$value = $this->getHttpRequest()->getQuery("sSearch_".$c);
			
			if( !empty($value) AND !is_null($value) ) $filters[$col] = $value;
		}
		
		$q = $this->getHttpRequest()->getQuery("sSearch");
		if( !empty($q) AND !is_null($q) ) $filters['q'] = $q;
		
		$orderDir = $this->getHttpRequest()->getQuery("sSortDir_0");
		if( $orderDir != 'asc' AND $orderDir != 'desc' ) $orderDir = null;
		
		$list = $this->getRepository()->getList( $filters, $offset, $perPage, $orderBy, $orderDir );
		
		$this->sendResponse(new \Nette\Application\Responses\JsonResponse( Array( 
			'sEcho' => (int)$this->getHttpRequest()->getQuery("sEcho"), 
			'iTotalRecords' => $this->getRepository()->countItemsBy( Array() ),
			'iTotalDisplayRecords' => $this->getRepository()->countItemsBy($filters),
			'aaData' => $list
			//'aaData' => $this->itemRepository->findBy($filters, $offset, $perPage, $orderBy, $orderDir)
		) ));
	}
	
	public function actionDetail($id=null)
    {
		if( !is_null($id) )
		{
			$this->item = $this->getRepository()->find($id);
			$this->getComponent('itemForm')
				->setDefaults(
					Array(
						'zip'			=> $this->item->zip,
					)
			);
		}
		else
		{
			
		}
	}
	
	public function renderDetail($id)
    {
		if($this->item) $this->template->item = $this->item;
    }
	
	public function actionDelete($id)
    {
		$this->item = $this->getRepository()->find($id);
		$this->getRepository()->delete($this->item);
		$this->redirect('Zip:default');
	}

	public function renderAdd()
    {
		$this->setView('detail');
    }

	

	
	protected function createComponentItemForm($name)
	{
		$form = new Form();
		
		$presenter = $this;
		$form->getElementPrototype()->class('form-horizontal');
		
		$rendererConfig = Array(
			'layout' => 'horizontal',
			'labels' => true,
			'submitFullWidth' => false,
			'submitAlign' => 'center',
			'leftCol' => 3,
			'rightCol' => 9,
		);
		$form->setRenderer( new \Nette\Forms\Rendering\BootstrapFormRenderer($rendererConfig) );

		$zip = $form->addText('zip', 'PSČ')
			->setRequired('PSČ je povinné!');
		$zip->getControlPrototype()->class('form-control');
		
		$form->onValidate[] = Array($this, 'validateForm');
		
		$send = $form->addSubmit('send', 'Uložit');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$values = $form->getValues();
			$httpRequest = $this->getContext()->getService('httpRequest');
			if( is_null($this->item) ) 
			{
				$presenter->item = new \Model\Entity\ZipEntity;
				
			}
			$presenter->item->zip			= $values['zip'];
			
			$presenter->getRepository()->persist($this->item);
			
			$presenter->redirect('Zip:default');
		};
		
		return $form;
	}
	
	
	public function validateForm($form)
	{
		$values = $form->getValues();
		$exists = $this->getRepository()->findOneBy( Array('zip'=>$values['zip']) );
		if ($exists AND $exists->id != $this->item->id) 
		{
			$form->addError('Zadané PSČ již v databázi existuje.');
			return false;
		}
		return true;
	}
	

	private function getItems()
	{
		$vp = $this['vp'];
				
	    $paginator = $vp->paginator;
	    $paginator->itemsPerPage = $this->static->itemsPerPage;
	    $paginator->itemCount = $this->itemRepository->countAll();
		return $this->itemRepository->getList( Array(), $paginator->offset, $paginator->itemsPerPage, 'name', 'ASC' );
		//$ids = $this->itemRepository->findIdsBy( Array(), $paginator->offset, $paginator->itemsPerPage, 'name', 'ASC' );
		
		$items = Array();
		foreach ($ids as $o => $id) 
		{
			$items[] = $this->itemRepository->getOneShort($id, $this->lang_id); 
		}
		return $items;
	}
	
	
	protected function getRepository()
	{
		return $this->zipRepository;
	}
	
}
