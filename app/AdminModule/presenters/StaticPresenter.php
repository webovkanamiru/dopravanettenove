<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use App\AdminModule\Presenters\BasePresenter;
use Nette\Application\UI\Form;

class StaticPresenter extends BasePresenter
{
	public $row;
	
	public function renderDefault()
	{
		$this->template->items	= $this->getItems();
	}
	
	public function actionDetail($id)
	{
		$this->row = $this->staticRepository->find($id);
		$this->getComponent('staticForm')
			->setDefaults( Array( 
				'name' => $this->row->name,
				'type' => $this->row->type,
				'text' => $this->row->text,
			) );
		
		if($this->row->type=='template') 
		{
			$form = $this->getComponent('staticForm');
			//$form['text']->setType('textArea');
					
		}
	}
	
	public function renderDetail($id)
	{
//			if( $this->row->type == 'text' ) $this->setView('richtext');
//		elseif( $this->row->type == 'timestamp' ) $this->setView('timestamp');
//		$this->setView('detail');
		$this->template->item = $this->row;
	}
	
	
	/**
	 * @param Form $form
	 */
	public function updateRow(\App\Forms\BaseForm $form)
	{
		$values = $form->getValues();
		
		if( $this->row->type == \Model\Entity\StaticEntity::TYPE_TEXT  ) $this->row->text = $values->text;
		if( $this->row->type == \Model\Entity\StaticEntity::TYPE_TIMESTAMP  ) 
		{
			$date = new \DateTime;
			$date->setTimestamp( strtotime($values->timestamp) );
			$this->row->timestamp = $date;
		}
		$this->staticRepository->persist($this->row);
		$this->flashMessage('Uloženo', 'success');
		$this->redirect('Default:default');
	}
	
	
	protected function createComponentStaticForm($name)
	{
		$form = new Form();
		$presenter = $this;
		$form->getElementPrototype()->class('form-horizontal');
		
		$rendererConfig = Array(
			'layout' => 'horizontal',
			'labels' => true,
			'submitFullWidth' => false,
			'submitAlign' => 'center',
			'leftCol' => 3,
			'rightCol' => 9,
		);
		$form->setRenderer( new \Nette\Forms\Rendering\BootstrapFormRenderer($rendererConfig) );
		
		$name = $form->addText('name','Název' )
			->setRequired('Název je povinný!');
		$name->getControlPrototype()->class('form-control');
		
		$type = $form->addText('type','Typ' )
			->setDisabled();
		$type->getControlPrototype()->class('form-control');
		
		$text = $form->addTextArea('text', 'Text' )
			->setRequired('Text je povinný!');
		$text->getControlPrototype()->class('form-control');
		
		$send = $form->addSubmit('send', 'Uložit');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onValidate[] = Array($this, 'validateStaticForm');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$mode='edit';
			$values = $form->getValues();
			$httpRequest = $this->getContext()->getService('httpRequest');
			if( is_null($presenter->row) ) 
			{
				$mode='insert';
				$presenter->row = new \Model\Entity\StaticEntity;
			}
			$presenter->row->name = $values['name'];
			$presenter->row->text = str_replace( Array("&gt;","<code>","</code>"), Array(">","",""), $values['text']);
			$presenter->staticRepository->persist($presenter->row);
			
			$presenter->cache->clean(array(
				Nette\Caching\Cache::TAGS => array("static"),
			));
			
			$presenter->redirect('Static:default');
		};
		return $form;
	}
	
	//zkontroluje jestli uz neexistuje dany vyrobce
	public function validateStaticForm($form)
	{
		$values = $form->getValues();
		$existing = $this->staticRepository->getOneBy( Array('name'=>$values['name']) );
				
		if(is_null($this->row) AND $existing) { // validační podmínka
			$form->addError('Toto nastavení je již vloženo.');
		}
	}
	
	protected function createComponentRichTextForm($name)
	{
		$form = $this->richTextFormFactory->create( $this->row );
		$form->onSuccess[] = $this->updateRow;
		return $form;
	}
	
	protected function createComponentTimestampForm($name)
	{
		$form = $this->timestampFormFactory->create( $this->row );
		$form->onSuccess[] = $this->updateRow;
		return $form;
	}
	
	private function getItems()
	{
		return $this->staticRepository->loadAll();
	}

	//  \Tracy\Debugger::fireLog('Hello World');
}
