<?php

namespace App\Templating;

use Model\Entity\ItemEntity;
use App\Cart\CartItem;
use Model\Entity\PageEntity;
use Nette\Application\Application;
use Nette\Object;
//use Netmo\Application\Settings\SystemSettings;
use Model\Entity\OrderEntity;
use Model\Entity\OrderItemEntity;
//use Netmo\Domain\Product\ImageEntity;
//use Netmo\Domain\Pages\PageImageEntity;

use Nette\Utils\Strings;

/**
 * Template helpers
 *
 */
class Helpers extends Object
{
	/**
	 * @inject
	 * @var Application
	 */
	private $application;

	/**
	 * @inject
	 * @var SystemSettings
	 */
	private $systemSettings;
	
	function __construct(Application $application)
    {
        $this->application = $application;
    }

	/**
	 * @param string $helper
	 * @return \Nette\Callback
	 */
	public function loader($name, $s, $r=null)
	{
		if (method_exists($this, $name) ) return $this->$name($s, $r);
		else return null;
	}
	
	public static function common($filter, $value)
    {
        if (method_exists(__CLASS__, $filter)) {
            $args = func_get_args();
            array_shift($args);
            return call_user_func_array([__CLASS__, $filter], $args);
        }
    }



	public function productLink(ProductEntity $product)
	{
		return $this->application->getPresenter()
			->link("//:Frontend:Product:detail", array(
				'id'    => $product->getId(),
				'slug'  => Strings::webalize($product->productTrans[0]->getName())
			));
	}
	public function cartItemLink(CartItem $cartItem)
	{
		return $this->application->getPresenter()
			->link("//:Frontend:Product:detail", array(
				'id'    => $cartItem->getProductId(),
				'slug'  => Strings::webalize($cartItem->getProductName())
			));
	}

	public function productImageLink(ImageEntity $imageEntity, $with = 0, $height = 0)
	{
		return $this->application->getPresenter()
			->link("//:Frontend:Image:renderProduct", array(
				'id'    => $imageEntity->getId(),
				'name'  => $imageEntity->getName(),
				'width' => $with,
				'height'=> $height
			));
	}
	
	public function pageImageLink(PageImageEntity $pageImageEntity, $with = 0, $height = 0)
	{
		return $this->application->getPresenter()
			->link("//:Frontend:Image:renderPage", array(
				'id'    => $pageImageEntity->getId(),
				'name'  => $pageImageEntity->getName(),
				'width' => $with,
				'height'=> $height
			));
	}
	
	public function carouselImageLink(CarouselImageEntity $carouselImageEntity, $with = 0, $height = 0)
	{
		return $this->application->getPresenter()
			->link("//:Frontend:Image:renderCarousel", array(
				'id'    => $carouselImageEntity->getId(),
				'name'  => $carouselImageEntity->getName(),
				'width' => $with,
				'height'=> $height
			));
	}

	

	public function priceFormat($value) {
		if( $langId = $this->application->getPresenter()->getLangId() == 1 ) return number_format( $value, 0, ",", " ");
		else return number_format( $value, 2, ".", " ");
	}
	
	public function currency() {
		if( $langId = $this->application->getPresenter()->getLangId() == 1 ) return 'Kč';
		else return "&euro;";
	}

	public function calculateVatPriceProduct($itemRow, $count = 1) {
		return  $this->priceFormat( $itemRow->price * $this->calculateVat($itemRow->vat) * $count );
	}
	
	public static function calculateVatPrice($price, $vat, $count = 1) {
		return $price * self::calculateVat($vat) * $count;
	}

	
	/**
	 * @param int $vat
	 * @return float
	 */
	public static function calculateVat($vat) {
		if (empty($vat)) {
			return 1;
		}
		return (float) 1 + ($vat / 100);
	}
	
	public static function calculateVatPrice2($price, $lang_id, $vat, $count = 1) {
		$price = $price * self::calculateVat($vat) * $count;
		if( $lang_id == 1 ) return round($price);
		else return round($price,2);
	}
	public static function round($price, $lang_id) {
		if( $lang_id == 1 ) return round($price);
		else return round($price,2);
	}

	public static function currencyByLang($foo, $lang_id) {
		if( $lang_id == 1 ) return 'Kč';
		else return "&euro;";
	}
	
	
	
	public function vatPrice(OrderItemEntity $orderItemEntity) {
		return $this->calculateVat($orderItemEntity->getVat()) * $orderItemEntity->getPrice() * $orderItemEntity->getCount();
	}

	public function dphSummary(OrderEntity $orderEntity) {
		$dphSum = array();
		/** @var OrderItemEntity $orderItem */
		foreach ($orderEntity->getOrderItems() as $orderItemEntity) {
			$dphSumVal = (float) $dphSum[$orderItemEntity->getVat()];
			$dphSumVal += ($this->vatPrice($orderItemEntity) - ($orderItemEntity->getPrice() * $orderItemEntity->getCount()));
			$dphSum[$orderItemEntity->getVat()] = $dphSumVal;
		}

		return $dphSum;
	}

	public function total(OrderEntity $orderEntity) {
		$price = 0;

		/** @var OrderItemEntity $orderItem */
		foreach ($orderEntity->getOrderItems() as $orderItemEntity) {
			$price += ($orderItemEntity->getPrice() * $orderItemEntity->getCount());
		}

		return $price;
	}

	public function totalAmount(OrderEntity $orderEntity) {
		$price = 0;

		/** @var OrderItemEntity $orderItem */
		foreach ($orderEntity->getOrderItems() as $orderItem) {
			$price += $orderItem->getPrice() * $orderItem->getCount();
		}

		return $price;
	}
	
	//nice - formatovani na de. mista+oddelovac tisicu
	//withCurrency - pridat znacku meny
	public function calculateCurrency($price, $nice=false, $withCurrency=false) {
		if( $this->application->getPresenter()->getLangId() > 1 ) $price = $price / $this->application->getPresenter()->exchangeRate;
		
		if( $nice ) $price = $this->priceFormat($price);
		
		if( $withCurrency AND $this->application->getPresenter()->getLangId() > 1 ) return "&euro;". $price;
		elseif( $withCurrency AND $this->application->getPresenter()->getLangId() == 1 ) return $price .' Kč';
		else return $price;
	}
	
	//nice - formatovani na de. mista+oddelovac tisicu
	//withCurrency - pridat znacku meny
	public function calculateIntoCurrency($price, $currency=1, $nice=false, $withCurrency=false) {
		if( $currency > 1 ) $price = $price / $this->application->getPresenter()->exchangeRate;
		
			if( $nice AND $currency == 1 ) $price = number_format( $price, 0, ",", " ");
		elseif( $nice AND $currency  > 1 ) $price = number_format( $price, 2, ".", " ");
		
		if( $withCurrency AND $currency > 1 ) return "&euro; ". $price;
		elseif( $withCurrency AND $currency == 1 ) return $price .' Kč';
		else return $price;
	}
	
	public function timeAgo(\Model\Entity\NotificationEntity $eventlog)
	{
		$diff = time() - strtotime($eventlog->date);
		
		if( $diff < 10 ) return 'právě nyní';
		elseif( $diff < 60 ) return 'před '. $diff .' vteřinami';
		elseif( $diff < 60*60 ) return 'před '. round($diff/60) .' minutami';
		elseif( $diff < 3*60*60 ) return 'před '. round($diff/60/60) .' hodinami';
		elseif( date('j.n.Y') == date('j.n.Y', strtotime($eventlog->date)) ) return 'dnes';
		return date('j.n.Y', strtotime($eventlog->date));
	}
	
	public function getVariantName($item, $variantId) {
		$variant = $this->application->getPresenter()->itemRepository->getVariantById($variantId);
		
		if($variant) return $variant->name .' '. $variant->value;
		else return false;
	}
	
	public function getPriceOfVariant($item, $variantId, $count=1) {
		$variant = $this->application->getPresenter()->itemparamvaluepriceRepository->findOneBy( Array('item_paramvalue_id'=>$variantId) );
		
		if($variant) return self::priceFormat($variant->price * ((100+$item->vat)/100));
		else return self::priceFormat(self::calculateVatPrice($item->price, $item->vat, $count), 1);
	}
	
	public static function myUcFirst($string) {
		
		if( strlen($string) > 0 )
		{
			$string = mb_strtoupper( mb_substr($string, 0, 1, 'UTF-8'), 'UTF-8' ) . mb_substr($string, 1, null, 'UTF-8');
		}
		return $string;
	}
	
	
	
}