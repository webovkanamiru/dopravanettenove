<?php
namespace App\Templating;

use Latte\Template;

class StringTemplate extends Template
{
        public $content;

        /**
         * Renders template to output.
         * @return void
         */
        public function render()
        {
                
                        if (!$this->getFilters()) {
                                $this->onPrepareFilters($this);
                        }

                        $content = $this->compile($this->content);
                
        }
        
        public function setString($string) 
        {
        	$this->content = $string;
        }
}