<?php

namespace App\Mail;

use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;
use App\Templating\Helpers;
use Model\Repository\StaticRepository;

class MyMail extends \Nette\Application\UI\Control {
	
	private $mail;
	private $mailer;
	private $smtpMailer;
	private $mailerType;
	private $staticRepository;
	private $helpers;
	
	function __construct( SendmailMailer $sendmailer, SmtpMailer $smtpmailer, $mailerType='mail', StaticRepository $staticRepository, Helpers $helpers)
    {
        $this->mailer = $sendmailer;
        $this->smtpMailer = $smtpmailer;
        $this->mailerType = $mailerType;
        $this->helpers = $helpers;
        $this->staticRepository = $staticRepository;
		
//		$this->mailer = new \Nette\Mail\SmtpMailer(Array(
//			'host'		=> $this->staticRepository->findByCode('smtpHost')->text,
//			'username'	=> $this->staticRepository->findByCode('mailSender')->text,
//			'password'	=> $this->staticRepository->findByCode('smtpPassword')->text,
//			//'secure'	=> 'ssl',
//		));
    }
	
	public function createMailFromStatic($staticCode, $data=null)
	{
		$this->mail = new Message;
		
		$template = $this->staticRepository->findByCode($staticCode);
		$heading = $template->name;
		$body    = $template->text;
		
		$latte = new \Latte\Engine;
		$latte->setLoader(new \Latte\Loaders\StringLoader());
		$latte->setTempDirectory(__DIR__.'/../../../temp/cache/latte');
		$latte->addFilter(NULL, 'App\Templating\Helpers::common');
		$body = $latte->renderToString( $template->text, Array('order'=>$data, 'static'=>$this->staticRepository->findAll()) );
		//die($body);
		
		$latte = new \Latte\Engine;
		$latte->setLoader(new \Latte\Loaders\StringLoader());
		$latte->setTempDirectory(__DIR__.'/../../../temp/cache/latte');
		$latte->addFilter(NULL, 'App\Templating\Helpers::common');
		$subject = $latte->renderToString( $template->name, Array('order'=>$data, 'static'=>$this->staticRepository->findAll()) );
		
		$this->mail->setFrom( $this->staticRepository->findByCode('webName')->text .' <'. $this->staticRepository->findByCode('mailSender')->text .'>' )
			->addTo( $data->pickupEmail )
			->setSubject( $subject )
			->setHtmlBody( $body );
	}
	
	public function createMailFromString($body, $subject, $recipient, $data=null)
	{
		$this->mail = new Message;
		$this->mail->setFrom( $this->staticRepository->findByCode('webName')->text .' <'. $this->staticRepository->findByCode('mailSender')->text .'>' )
			->addTo($recipient)
			->setSubject( $subject )
			->setHtmlBody( $body );
	}
	
	public function send()
	{
		if( $this->mailerType == 'mail' ) $this->mailer->send($this->mail);
		elseif( $this->mailerType == 'smtp' ) $this->smtpMailer->send($this->mail);
	}
	
	public function addBcc($address)
	{
		$this->mail->addBcc($address);
	}
	
	public function addAttachment($path){
		$this->mail->addAttachment($path);
	}
	
}
