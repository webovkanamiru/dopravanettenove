<?php
namespace Model;

use Nette\Object;

class ConfigParameters extends Object
{
	public $vat;
	public $smallImageWidth;
	public $smallImageHeight;
	public $mediumImageHeight;
	public $largeImageHeight;
	public $sourceImageUrl;
	public $wwwDir;
	public $appDir;
	public $tempDir;
	public $sklikUsername;
	public $sklikPassword;
	public $sklikEndpoint;
	public $sklikManagedUserId;
	public $mailerType;
	public $cssSource;
	

	public function __construct(array $parameters)
	{
		$args = func_get_args();
		
		if( is_array($args) AND isset($args[0]) AND is_array($args[0]) )
		{
			foreach ($args[0] as $key => $value) 
			{
				if( property_exists( $this, $key ) ) $this->{$key} = $value;
			}
		}
	}
}
?>
