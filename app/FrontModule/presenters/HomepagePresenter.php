<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;


class HomepagePresenter extends BasePresenter
{

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}
	
	public function renderOrder()
	{
		$this->template->occupiedList = $this->occupieddateRepository->getList();
	}
	
	protected function createComponentOrderForm()
	{
		$form = new Form();
		$presenter = $this;
		
		//informace o zbozi
		$container = $form->addContainer('items');
		
		$volume = $container->addText('volume', 'Předpokládaný objem')
			->setOption('description', 'm3')
			->setRequired('Vyplňte předpokládaný objem')
			->addRule(Form::FLOAT, 'Předpokládaný objem musí být číselný');
		$volume->getControlPrototype()->class('form-control');
		
		$weight = $container->addText('weight', 'Předpokládaná hmotnost')
			->setOption('description', 'kg')
			->addRule(Form::FILLED, 'Vyplňte předpokládanou hmotnost')
			->addRule(Form::FLOAT, 'Předpokládaná hmotnost musí být číselná');
		$weight->getControlPrototype()->class('form-control');
		
		$qty = $container->addText('qty', 'Počet kusů v zásilce')
			->addRule(Form::FILLED, 'Vyplňte počet kusů v zásilce')
			->addRule(Form::FLOAT, 'Počet kusů v zásilce musí být číselný');
		$qty->getControlPrototype()->class('form-control');
		
		$ordernumber = $container->addText('ordernumber', 'Číslo objednávky u dodavatele')
			->addRule(Form::FILLED, 'Vyplňte číslo objednávky u dodavatele');
		$ordernumber->getControlPrototype()->class('form-control');
		
		$paid = $container->addRadioList('paid', 'Zaplaceno', Array(0=>'ne',1=>'ano'));

		
		//informace o vyzvednuti
		$container = $form->addContainer('pickup');
		
		$shop = $container->addSelect('shop', 'Vyberte obchod', $this->shopRepository->pairs($this->shopRepository->findBy( Array('approved'=>\Model\Entity\ShopEntity::APPROVED_YES) ), 'id', 'name') )
			->setPrompt('manuální zadání');
		$shop->addCondition(Form::EQUAL, false)
			->toggle('container-setshop');
		$shop->getControlPrototype()->class('form-control');
		
		$name = $container->addText('name', 'Název obchodu');
		$name->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Název obchodu je povinný');
		$name->getControlPrototype()->class('form-control');
		
		$centre = $container->addText('centre', 'Nákupní centrum');
		$centre->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Nákupní centrum je povinné');
		$centre->getControlPrototype()->class('form-control');
		
		$street = $container->addText('street', 'Ulice a č.p.');
		$street->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Ulice je povinná');
		$street->getControlPrototype()->class('form-control');
		
		$city = $container->addText('city', 'Město');
		$city->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Město je povinné');
		$city->getControlPrototype()->class('form-control');
		
		$zip = $container->addText('zip', 'PSČ', 5);
		$zip->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->addRule(Form::FILLED, 'PSČ je povinné!')
			->addRule(Form::MIN_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule(Form::MAX_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule( Array($this,'isValidZip'), 'Zadané PSČ vyzvednutí není platné');
		$zip->getControlPrototype()->class('form-control');
		
		$phone = $container->addText('phone', 'Telefon');
		$phone->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Telefoní číslo je povinné!');
		$phone->setEmptyValue('+420')
			->setType('phone');
		$phone->getControlPrototype()->class('form-control');
		
		$email = $container->addText('email', 'Email');
		$email->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->setRequired('Emailová adresa je povinná')
			->addRule(Form::EMAIL, 'Emailová adresa je povinná');
		$email->setType('mail');
		$email->getControlPrototype()->class('form-control');
		
		$contactPerson = $container->addText('contactPerson', 'Kontaktní osoba');
		$contactPerson->addConditionOn($form['pickup']['shop'], Form::EQUAL, false)
			->addRule(Form::FILLED, 'Kontaktní osoba je povinná');
		$contactPerson->getControlPrototype()->class('form-control');
		
		
		
		$pickupDate = $container->addText('pickupDate', 'Datum vyzvednutí')
			->setRequired('Zvolte datum vyzvednutí');
		$pickupDate->getControlPrototype()->class('form-control');
		
		$pickupTime = $container->addText('pickupTime', 'Čas vyzvednutí');
		$pickupTime->getControlPrototype()->class('form-control');
		
		
		
		//informace o vylozeni
		$container = $form->addContainer('destination');
		
		$company = $container->addText('company', 'Firma')
				->setRequired('Zvolte firmu');
		$company->getControlPrototype()->class('form-control');
		
		$name = $container->addText('name', 'Jméno a příjmení')
				->setRequired('Zvolte jméno a příjmení');
		$name->getControlPrototype()->class('form-control');
		
		$street = $container->addText('street', 'Ulice a č.p.')
			->setRequired('Ulice je povinná');
		$street->getControlPrototype()->class('form-control');
		
		$city = $container->addText('city', 'Město')
			->setRequired('Město je povinné');
		$city->getControlPrototype()->class('form-control');
		
		$zip = $container->addText('zip', 'PSČ', 5)
			->addRule(Form::FILLED, 'PSČ je povinné!')
			->addRule(Form::MIN_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule(Form::MAX_LENGTH, 'PSČ musí mít %d znaků', 5)
			->addRule( Array($this,'isValidZip'), 'Zadané PSČ doručení není platné');
		$zip->getControlPrototype()->class('form-control');

		$destinationDate = $container->addText('destinationDate', 'Datum doručení');
		$destinationDate->getControlPrototype()->class('form-control');
		
		$destinationTime = $container->addText('destinationTime', 'Čas doručení');
		$destinationTime->getControlPrototype()->class('form-control');
		
		
		//informace o platbe
		$container = $form->addContainer('payment');
		
		$whoPay = $container->addRadioList('whoPay', 'Plátce dopravy', Array(\Model\Entity\OrderEntity::WHOPAY_SENDER=>'objednavatel', \Model\Entity\OrderEntity::WHOPAY_RECIPIENT=>'příjemce'))
			->setRequired('Zvolte plátce dopravy');
		
		$paymentMethod = $container->addRadioList('paymentMethod', 'Způsob platby', Array( \Model\Entity\OrderEntity::PAYMENT_CASH=>'hotově', \Model\Entity\OrderEntity::PAYMENT_CARD=>'platební kartou'))
			->setRequired('Zvolte způsob platby');
		
		$service = $form->addCheckboxList('service', 'Služby', $this->serviceRepository->pairs( $this->serviceRepository->findAll(), 'id', 'nameprice'));
		
		
		$send = $form->addSubmit('send', 'Objednat');
		$send->getControlPrototype()->class('btn btn-lg btn-primary');
		
		$form->onValidate[] = Array($this, 'validateOrderForm');
		
		$form->onSuccess[] = function (Form $form) use ($presenter) {
			$values = $form->getValues();
			
			//-------------- vytvoreni objednavky -------------------------------------------------------
			$eOrder = new \Model\Entity\OrderEntity;
			$eOrder->slotFrom = null;
			
			//pickup
			preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($values['pickup']['pickupDate']), $matchesPickupDate);
			preg_match('/([0-9]{1,2}):([0-9]{1,2})/', trim($values['pickup']['pickupTime']), $matchesPickupTime);
			if( count($matchesPickupDate) == 4 AND count($matchesPickupTime) == 3 ) 
			{
				$eOrder->pickupDate = new \DateTime( $matchesPickupDate[3] .'-'. $matchesPickupDate[2] .'-'. $matchesPickupDate[1] .' '. $matchesPickupTime[1] .':'. $matchesPickupTime[2] .':00' );
				
				//urceni minuty vyzvednuti (zaokrouhluji dolu na 15 minut)
					if( $matchesPickupTime[2] >= 0  AND $matchesPickupTime[2] < 15 ) $slotFromMinutes = '00';
				elseif( $matchesPickupTime[2] >= 15 AND $matchesPickupTime[2] < 30 ) $slotFromMinutes = '15';
				elseif( $matchesPickupTime[2] >= 30 AND $matchesPickupTime[2] < 45 ) $slotFromMinutes = '30';
				elseif( $matchesPickupTime[2] >= 45 AND $matchesPickupTime[2] < 60 ) $slotFromMinutes = '45';
				
				$eOrder->slotFrom = new \DateTime( $matchesPickupDate[3] .'-'. $matchesPickupDate[2] .'-'. $matchesPickupDate[1] .' '. $matchesPickupTime[1] .':'. $slotFromMinutes .':00' );
			}
			else $eOrder->pickupDate		= new \DateTime;
			
			if( count($matchesPickupTime) == 3 ) $eOrder->pickupTime = $matchesPickupTime[1] .':'. $matchesPickupTime[2] .':00';
			else $eOrder->pickupTime = null;
			
			//destination
			preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($values['destination']['destinationDate']), $matchesDestinationDate);
			preg_match('/([0-9]{1,2}):([0-9]{1,2})/', trim($values['destination']['destinationTime']), $matchesDestinationTime);
			if( count($matchesDestinationDate) == 4 ) $eOrder->destinationDate = new \DateTime( $matchesDestinationDate[3] .'-'. $matchesDestinationDate[2] .'-'. $matchesDestinationDate[1] );
			else $eOrder->destinationDate		= new \DateTime;
			
			if( count($matchesDestinationTime) == 3 ) $eOrder->destinationTime = $matchesDestinationTime[1] .':'. $matchesDestinationTime[2] .':00';
			else $eOrder->destinationTime = null;
			
			
			$eOrder->volume			= (float)$values['items']['volume'];
			$eOrder->weight			= (float)$values['items']['weight'];
			$eOrder->qty			= (int)$values['items']['qty'];
			$eOrder->paid			= (int)$values['items']['paid'];
			$eOrder->ordernumber	= trim($values['items']['ordernumber']);
			
			$eOrder->whoPay			= trim($values['payment']['whoPay']);
			$eOrder->paymentMethod	= trim($values['payment']['paymentMethod']);
			$eOrder->slotDuration	= 120;
			
			if( !is_null($values['pickup']['shop']) )
			{
				//vyplnim hodnotama z `shop`
				$eShop = $presenter->shopRepository->find($values['pickup']['shop']);
				if( $eShop )
				{
					$eOrder->pickupName		= $eShop->name;
					$eOrder->pickupCentre	= $eShop->centre;
					$eOrder->pickupStreet	= $eShop->street;
					$eOrder->pickupCity		= $eShop->city;
					$eOrder->pickupZip		= $eShop->zip;
					$eOrder->pickupPhone	= $eShop->phone;
					$eOrder->pickupContactPerson	= $eShop->contactPerson;
					$eOrder->pickupEmail	= $eShop->email;
				}
			}
			else
			{
				$eOrder->pickupName		= trim($values['pickup']['name']);
				$eOrder->pickupCentre	= trim($values['pickup']['centre']);
				$eOrder->pickupStreet	= trim($values['pickup']['street']);
				$eOrder->pickupCity		= trim($values['pickup']['city']);
				$eOrder->pickupZip		= trim($values['pickup']['zip']);
				$eOrder->pickupPhone	= trim($values['pickup']['phone']);
				$eOrder->pickupContactPerson	= trim($values['pickup']['contactPerson']);
				$eOrder->pickupEmail	= trim($values['pickup']['email']);
				
				//zalozit `shop`
				$eShop = new \Model\Entity\ShopEntity;
				$eShop->name			= trim($values['pickup']['name']);
				$eShop->centre			= trim($values['pickup']['centre']);
				$eShop->street			= trim($values['pickup']['street']);
				$eShop->city			= trim($values['pickup']['city']);
				$eShop->zip				= trim($values['pickup']['zip']);
				$eShop->phone			= trim($values['pickup']['phone']);
				$eShop->contactPerson	= trim($values['pickup']['contactPerson']);
				$eShop->email			= trim($values['pickup']['email']);
				$presenter->shopRepository->persist($eShop);
			}
			$eOrder->destinationCompany	= trim($values['destination']['company']);
			$eOrder->destinationName	= trim($values['destination']['name']);
			$eOrder->destinationStreet	= trim($values['destination']['street']);
			$eOrder->destinationCity	= trim($values['destination']['city']);
			$eOrder->destinationZip		= trim($values['destination']['zip']);
			
			$eOrder->basePrice			= $presenter->static->basePrice;
			
			$presenter->orderRepository->persist($eOrder);
			
			//service
			if( isset($values['service']) AND count($values['service']) > 0 )
			{
				foreach ($values['service'] as $id) 
				{
					$service = $this->serviceRepository->find($id);
					if($service) $eOrder->addToService($service);
				}
				$presenter->orderRepository->persist($eOrder);
			}
			
			
			//priradit truck - pokud je vyplneno aspon jedno datum
			if( !is_null($eOrder->pickupDate) OR !is_null($eOrder->deliveryDate) )
			{
				$result = $presenter->orderRepository->findAvailableTruck( (isset($eOrder->pickupDate)) ? $eOrder->pickupDate : $eOrder->deliveryDate, $eOrder->slotFrom, $eOrder->slotDuration*60, $this->static->workHoursBegin, $this->static->workHoursEnd);
				
				if( !is_null($result) ) 
				{
					//if( $result['slotFrom'] ) echo "<br><b>truck_id = ".$result['truck_id']."</b>, from = ". date('H:i', $result['slotFrom']) ."<br>";
					//else echo "<br><b>truck_id = ".$result['truck_id']."</b><br>";
					
					$eOrder->truck = $presenter->truckRepository->find($result['truck_id']);
					$slotFrom = new \DateTime();
					$slotFrom->setTimestamp($result['slotFrom']);
					$eOrder->slotFrom = $slotFrom;
				}
				$presenter->orderRepository->persist($eOrder);
			}
			$presenter->flashMessage('Objednávka odeslána.', 'success');
			//exit;
			
			
			//-------------- odeslani emailu -------------------------------------------------------
			$this['myMail']->createMailFromStatic('orderCreated', $eOrder);
			$this['myMail']->addBcc($presenter->static->primaryEmail);
			$this['myMail']->send();
			
			$presenter->redirect('Homepage:default');
		};
		
		return $form;
	}
	
	
	public function validateOrderForm($form)
	{
		$values = $form->getValues();
		
		//kontrola jestli delivery time neni drive nez pickup time
		if( 
			!is_null( $values['pickup']['pickupTime'] ) AND 
			!is_null( $values['pickup']['pickupDate'] ) AND 
			!is_null( $values['destination']['destinationTime'] ) AND 
			!is_null( $values['destination']['destinationDate'] ) 
		)
		{
			if( 
				preg_match('/([0-9]{2}):([0-9]{2})/', $values['pickup']['pickupTime'], $matchesPickupTime) AND 
				preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', $values['pickup']['pickupDate'], $matchesPickupDate) AND
				preg_match('/([0-9]{2}):([0-9]{2})/', $values['destination']['destinationTime'], $matchesDestinationTime) AND
				preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', $values['destination']['destinationDate'], $matchesDestinationDate)
			)
			{
				$pickup = new \DateTime( $matchesPickupDate[1] .'-'. $matchesPickupDate[2] .'-'. $matchesPickupDate[3] .' '. $matchesPickupTime[1] .':'. $matchesPickupTime[2] .':00' );
				$destination = new \DateTime( $matchesDestinationDate[1] .'-'. $matchesDestinationDate[2] .'-'. $matchesDestinationDate[3] .' '. $matchesDestinationTime[1] .':'. $matchesDestinationTime[2] .':00' );
				if( $pickup->getTimestamp() > $destination->getTimestamp() )
				{
					$form->addError('Čas doručení nemůže předcházet času vyzvednutí.');
				}
				
			}
		}
	}
	
	
	public function isValidZip($control)
    {
        $existingZip = $this->zipRepository->getOneBy( Array('zip'=>$control->value) );
		if( $existingZip ) return true;
		else return false;
    }
	
	//naplnit tabulku zip
	public function actionFillZip()
	{
		echo '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
		if( !function_exists('dbase_open') ) die('dbase extension not loaded');
		$file = '../app/dbf/svazpsc_6.dbf';
		if( !file_exists($file) ) die('file '. $file .' does not exists');
		
		$db = dbase_open($file, 0);
		$inserCounter=0;
		if($db) 
		{
			$record_numbers = dbase_numrecords($db);
			for ($i = 1; $i <= $record_numbers; $i++) 
			{
				$dRow = dbase_get_record_with_names($db, $i);
				
				$name = iconv('CP852', 'UTF-8', trim($dRow['NAZEV']));
				if( $name == 'neobsazeno' OR $name == 'záloha' OR $name == 'technologické') continue;
				
				$row = Array(
					'PSC'		=> trim($dRow['PSC']),
					'NAZ_POST'	=> $name,
				);
				//echo $name .'<br>';
				$posta = $this->zipRepository->findBy( Array('zip' => trim($dRow['PSC']) ) );
                if(!$posta) 
				{
					echo "row #$i: ". trim($dRow['PSC']) ."<br>";
					$eZip = new \Model\Entity\ZipEntity;
					$eZip->zip = trim($dRow['PSC']);
					$this->zipRepository->persist($eZip);
					$inserCounter++;
				}
			}
			dbase_close($db);
		}
		else die('unable to open file '. $file);
		echo "$inserCounter rows inserted<br>";
		$this->terminate();
	}

}
