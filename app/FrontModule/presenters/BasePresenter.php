<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\Model;
use Model\Repository\ShopRepository;
use Model\Repository\ZipRepository;
use Model\Repository\OrderRepository;
use Model\Repository\TruckRepository;
use Model\Repository\StaticRepository;
use Model\Repository\OccupieddateRepository;
use Model\Repository\ServiceRepository;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/** @var ShopRepository @inject */
    public $shopRepository;
	
	/** @var ZipRepository @inject */
    public $zipRepository;
	
	/** @var OrderRepository @inject */
    public $orderRepository;
	
	/** @var TruckRepository @inject */
    public $truckRepository;
	
	/** @var StaticRepository @inject */
    public $staticRepository;
	
	/** @var OccupieddateRepository @inject */
    public $occupieddateRepository;
	
	/** @var ServiceRepository @inject */
    public $serviceRepository;
	
	
	public $static;
	
	/** @var \Nette\Mail\SendmailMailer @inject */
    public $sendmailer;
	
	public $smtpmailer;
	
	/**
	 * @var \Model\ConfigParameters
	 * @inject
	 */
	public $configParametersModel;
	
	/** @var \App\Templating\Helpers @inject */
    public $helpers;
	
	function startup()
	{
		parent::startup();
		
		/** @var $this->static stdClass */
		$this->static = $this->staticRepository->findAll();
		
		$this->smtpmailer = new \Nette\Mail\SmtpMailer(Array(
			'host'		=> $this->static->smtpHost,
			'username'	=> $this->static->mailSender,
			'password'	=> $this->static->smtpPassword,
			//'secure'	=> 'ssl',
		));
	}
	
	protected function createComponentMyMail()
	{
		return new \App\Mail\MyMail($this->sendmailer, $this->smtpmailer, $this->configParametersModel->mailerType, $this->staticRepository, $this->helpers);
	}
}
