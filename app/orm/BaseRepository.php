<?php

namespace Model\Repository;

abstract class BaseRepository extends \LeanMapper\Repository
{
	//vrati jeden radek podle ID
	public function find($id)
	{
		// first part
		$row = $this->connection->select('*')
			->from($this->getTable())
			->where('id = %i', $id)
			->fetch();

		if ($row === false) {
			throw new \Exception('Entity was not found.');
		}
		// second part
		return $this->createEntity($row);
	}

	public function findAll()
	{
		return $this->createEntities(
			$this->connection->select('*')
				->from($this->getTable())
				->fetchAll()
		);
	}
	
	public function countAll()
	{
		return $this->connection->select('count(*)')
				->from($this->getTable())
				->fetchSingle();
	}

	
	
	//vrati radky podle kriterii
	public function findBy($filters, $offset=0, $perPage=100, $orderBy='id', $orderDir='ASC')
	{
		$select = $this->connection->select('*')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == 'in' AND is_array($value[1]) ) $select->where($key .' IN %in', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' LIKE %like~', $value);
		}
		if( !is_null($orderBy) AND !is_null($orderDir) ) $select->orderBy($orderBy.' '.$orderDir);
		$rows = ( !is_null($offset) AND !is_null($perPage) ) ? $select->fetchAll($offset,$perPage) : $select->fetchAll();
		
		return ($rows) ? $this->createEntities($rows) : null;
	}
	
	//vrati pocet radku podle kriterii
	public function countBy($filters)
	{
		$select = $this->connection->select('COUNT(*)')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' LIKE %like~', $value);
		}
		return $select->fetchSingle();
	}
	
	
	
	//vrati jeden radek podle kriterii
	public function getOneBy($filters)
	{
//		$db = debug_backtrace();
//		echo 'getOneBy() called by: '.$db[1]['function'].'<br>';
//		print_r($filters);
//		echo '<br>';
		$select = $this->connection->select('*')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %f', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' = %s', $value);
		}
		$row = $select->fetch();
		return ($row) ? $this->createEntity($row) : null;
	}
	
	//vrati jeden radek podle kriterii - textove retezce hleda pomoci LIKE
	public function findOneBy($filters)
	{
		$select = $this->connection->select('*')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == 'in' AND is_array($value[1]) ) $select->where($key .' IN %in', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' LIKE %like~', $value);
		}
		$row = $select->fetch();
		return ($row) ? $this->createEntity($row) : null;
		//return $this->createEntity( $select->fetch() );
	}
	
	
	//vraci maximalni ciselnou hodnotu sloupce
	public function getMax($column)
	{
		$max = $this->connection->select(' MAX('.$column.') ')
			->from($this->getTable())
			->fetchSingle();
		return ($max) ? $max : 0;
	}
	
	//============== vraci rows bez entit ====================
	
	public function findAllRows()
	{
		return $this->connection->select('*')
				->from($this->getTable())
				->fetchAll();
	}
	
	public function findRow($id)
	{
		return $this->connection->select('*')
				->from($this->getTable())
				->where('id = %i', $id)
				->fetch();
	}
	
	//vrati radky podle kriterii
	public function findRowsBy($filters, $offset=0, $perPage=100, $orderBy='id', $orderDir='ASC')
	{
		$select = $this->connection->select('*')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' LIKE %like~', $value);
		}
		return $select->orderBy($orderBy.' '.$orderDir)->fetchAll($offset,$perPage);
	}
	
	//vrati radek podle kriterii
	public function findRowBy($filters)
	{
		$select = $this->connection->select('*')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' LIKE %like~', $value);
		}
		return $select->fetch();
	}
	
	//vrati id radku podle kriterii
	public function findIdsBy($filters, $offset=0, $perPage=100, $orderBy='id', $orderDir='ASC')
	{
		$select = $this->connection->select('id')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
			if( is_array($value) AND $value[0] == '>=' ) $select->where($key .' >= %i', $value[1]);
			elseif( is_array($value) AND $value[0] == '<=' ) $select->where($key .' <= %i', $value[1]);
			elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
			elseif( is_null($value) ) $select->where($key .' IS NULL');
			elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
			else $select->where($key .' LIKE %like~', $value);
		}
		$ids = Array();
		$rows = $select->orderBy($orderBy.' '.$orderDir)->fetchAll($offset,$perPage);
		foreach ($rows as $row) $ids[] = $row->id;
		return $ids;
	}
	
	//vrati radky podle id
	public function findByIds($byCol, $ids)
	{
		$rows = $this->connection->select('*')
				->from($this->getTable())
				->where($byCol.' IN %in', $ids)
				->fetchAll();
		//return $rows;
		return $this->createEntities($rows);
	}
	
	
	
	//vrati radky v asociativnim poli, kde klicem je hodnota daneho sloupce, napr. `id`, tj. Array( 1=>Array(..), 2=>Array(..)  )
	public function withKeys($rows,$keyColumn)
	{
		$pairs = Array();
		foreach ($rows as $row) $pairs[ $row->{$keyColumn} ] = $row;
		return $pairs;
	}
	
	//vrati klasicke asocitivni pole key=>value
	public function pairs($rows, $keyColumn, $valueColumn)
	{
		if( !is_array($rows) ) return Array();
		$pairs = Array();
		foreach ($rows as $row) $pairs[ $row->{$keyColumn} ] = $row->{$valueColumn};
		return $pairs;
	}
	
	public function setMapper($mapper)
	{
		$this->mapper = $mapper;
	}
	
	
	//---- multijazycnost ----
	
	//vrati jeden radek v jednom jazyku
	public function findRowByLang($id, $lang_id, Array $nonTranslated, Array $translated)
	{
		$row = $this->connection->select('*, '. $this->getTable() .'.id AS id')
			->from($this->getTable())
			->leftJoin($this->getTable().'translation')->on( $this->getTable() .'.id = '. $this->getTable() .'translation.'. $this->getTable() .'_id')
			->where( $this->getTable() .'.id = %i', $id)
			->where( 'lang_id = %i', $lang_id)
			->fetch();
		//if(!$row) return false;
		$holder = new \Model\Entity\EntityHolder;
		$holder->setRow($row, $nonTranslated, $translated);
		return $holder;
	}
	
	
	//vrati jeden radek ve vsech jazycich
	public function findRowAllLangs($id, Array $nonTranslated, Array $translated)
	{
		$rows = $this->connection->select('*, '. $this->getTable() .'.id AS id')
			->from($this->getTable())
			->leftJoin($this->getTable().'translation')->on( $this->getTable() .'.id = '. $this->getTable() .'translation.'. $this->getTable() .'_id')
			->where( $this->getTable() .'.id = %i', $id)
			->fetchAll();
		//print_r($rows);
		//exit;
		$holder = new \Model\Entity\EntityHolder;
		$holder->setRowMultilang($rows, $nonTranslated, $translated);
		return $holder;
	}
	
	
	//vrati vsechny radky v jednom jazyku
	public function findAllRowsByLangId($lang_id, $orderBy='id ASC')
	{
		$rows = $this->connection->select('*, '. $this->getTable() .'.id AS id')
			->from($this->getTable())
			->leftJoin($this->getTable().'translation')->on( $this->getTable() .'.id = '. $this->getTable() .'translation.'. $this->getTable() .'_id')
			->where( 'lang_id = %i', $lang_id)
			->orderBy($orderBy)
			->fetchAll();

		//print_r($rows);
		$holder = new \Model\Entity\EntityHolder;
		$holder->setRows( $this->createEntities($rows) );
		//$holder->setRows($rows);
		return $holder;
	}
	
	//vrati dvojice v jednom jazyku - pro selecty
	public function getTranslatedPairs($lang_id, $valueColumnName='name')
	{
		return $this->connection->select($this->getTable() .'.id AS id, '.$valueColumnName)
			->from($this->getTable())
			->leftJoin($this->getTable().'translation')->on( $this->getTable() .'.id = '. $this->getTable() .'translation.'. $this->getTable() .'_id')
			->where( 'lang_id = %i', $lang_id)
			->fetchPairs('id', $valueColumnName);
	}
	
	public function insertMultilang( \LeanMapper\Entity $oEntity, \Nette\Utils\ArrayHash $values, Array $langs )
	{
		$this->connection->begin();
		$insertData1 = Array();
		foreach ($values as $columnName => $value)
		{
			if( in_array($columnName, $oEntity->getNottranslatedColumnNames() ) ) $insertData1[$columnName] = $value;
		}
		$this->connection->insert($this->getTable(), $insertData1)->execute();
		$page_id = $this->connection->getInsertId();
		
		foreach ($langs as $lang) 
		{
			$insertData = Array($this->getTable().'_id' => $page_id, 'lang_id' => $lang->id);
			foreach($oEntity->getTranslatedColumnNames() as $columnName) if( isset($values[$columnName.'_'.$lang->id]) ) $insertData[$columnName] = $values[$columnName.'_'.$lang->id];
			$this->connection->insert($this->getTable().'translation', $insertData)->execute();
		}
		$this->connection->commit();
	}
	
	
	public function updateMultilang( \LeanMapper\Entity $oEntity, $id, \Nette\Utils\ArrayHash $values, Array $langs )
	{
		$this->connection->begin();
		$data = Array();
		foreach ($values as $columnName => $value)
		{
			if( in_array($columnName, $oEntity->getNottranslatedColumnNames() ) ) $data[$columnName] = $value;
		}
		$this->connection->update($this->getTable(), $data)
			->where('[id] = %i', $id)
			->execute();
		
		foreach ($langs as $lang) 
		{
			$data = Array();
			foreach ($values as $columnName => $value)
			{
				if( preg_match('/([a-zA-Z]+)_([0-9]+)/', $columnName, $matches) )
				{
					if( $matches[2] == $lang->id AND in_array($matches[1], $oEntity->getTranslatedColumnNames() ) ) $data[$matches[1]] = $value;
				}
			}
			$this->connection->update($this->getTable().'translation', $data)
				->where('['. $this->getTable() .'_id] = %i AND [lang_id] = %i', $id, $lang->id)
				->execute();
		}
		$this->connection->commit();
	}

}
?>
