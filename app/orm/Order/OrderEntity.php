<?php

namespace Model\Entity;

use DateTime as PickupDate;
use DateTime as DestinationDate;
use DateTime as SlotFromDate;

use DateTime as DueDate;
use DateTime as InvoiceCreatedDate;
use DateTime as LastModifiedDate;

/**
 * @property int $id
 * 
 * @property TruckEntity|null $truck m:hasOne(truck_id)
 * @property ServiceEntity[] $service m:hasMany(order_id)
 * 
 * @property PickupDate $pickupDate
 * @property string|null $pickupTime
 * 
 * @property DestinationDate|null $destinationDate
 * @property string|null $destinationTime
 * 
 * @property SlotFromDate|null $slotFrom
 * @property int $slotDuration
 * 
 * @property float $volume
 * @property float $weight
 * @property int $qty
 * @property int $paid
 * @property string $whoPay m:enum(self::WHOPAY_*)
 * @property string $paymentMethod m:enum(self::PAYMENT_*)
 * @property string $ordernumber
 * 
 * @property string $pickupName
 * @property string|null $pickupCentre
 * @property string $pickupStreet
 * @property string $pickupCity
 * @property string $pickupZip
 * @property string $pickupPhone
 * @property string $pickupContactPerson
 * @property string $pickupEmail
 * 
 * @property string $destinationCompany
 * @property string $destinationName
 * @property string $destinationStreet
 * @property string $destinationCity
 * @property string $destinationZip
 * 
 * @property string|null $invoicePrefix
 * @property string|null $invoiceNumber
 * @property DueDate|null $dueDate
 * @property InvoiceCreatedDate|null $invoiceCreatedDate
 * @property LastModifiedDate|null $lastModifiedDate
 * @property int|null $invoiceSent=0
 * @property float $basePrice
 * 
 */
class OrderEntity extends \LeanMapper\Entity
{
	const WHOPAY_SENDER  = 'sender';
	const WHOPAY_RECIPIENT  = 'recipient';
	
	const PAYMENT_CASH  = 'cash';
	const PAYMENT_CARD  = 'card';
	
	public function getSlotTo() 
	{
		return ( is_null($this->row->slotFrom) ) ? null : $this->row->slotFrom->modify('+'. $this->row->slotDuration .' minutes');
	}
	
	public function getPrice() 
	{
		$price = $this->row->basePrice;
		
		foreach ($this->service as $service) $price += $service->price;
		return $price;
	}
	
	public function getNicePickupTime()
	{
		if( is_null($this->row->pickupTime) ) return null;
		else
		{
			$pt = new \DateTime($this->row->pickupTime);
			return date('H:i', $pt->getTimestamp());
		}
	}
	
	public function getNiceDestinationTime()
	{
		if( is_null($this->row->destinationTime) ) return null;
		else
		{
			$pt = new \DateTime($this->row->destinationTime);
			return date('H:i', $pt->getTimestamp());
		}
	}
	
	public function getNiceSlotTime()
	{
		if( is_null($this->row->slotFrom) ) return null;
		else
		{
			$pt = new \DateTime($this->row->slotFrom);
			return date('H:i', $pt->getTimestamp());
		}
	}
	
	function hasService(\Model\Entity\ServiceEntity $service)
	{
		foreach ($this->service as $entity) 
		{
			if( $entity->id == $service->id ) return true;
		}
		return false;
	}
	
	function getServiceIds()
	{
		$ids = Array();
		foreach ($this->service as $entity) 
		{
			$ids[] = $entity->id;
		}
		return $ids;
	}
	
}
?>
