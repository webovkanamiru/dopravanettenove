<?php

namespace Model\Repository;

use Model\Repository\BaseRepository;

class OrderRepository extends BaseRepository
{
	
	public function findAvailableTruck( \DateTime $date, $slotFrom, $slotDuration, $workHoursBegin, $workHoursEnd) 
	{
		//echo 'pickupDate = '. date('Y-m-d H:i',$date->getTimestamp()) .'<br>';
		//if( is_object($slotFrom) ) echo 'slotFrom = '. date('Y-m-d H:i', $slotFrom->getTimestamp()) .'<br>';
		//echo 'slotDuration = '. ($slotDuration/60) .'<br><br>';
		
		$shiftBegin = mktime($workHoursBegin[0], $workHoursBegin[1], 0, date('m',$date->getTimestamp()), date('d',$date->getTimestamp()), date('Y',$date->getTimestamp()));
		$shiftEnd   = mktime($workHoursEnd[0],   $workHoursEnd[1],   0, date('m',$date->getTimestamp()), date('d',$date->getTimestamp()), date('Y',$date->getTimestamp()));
		
		//nactu vsechny truck
		$trucks = $this->connection->select('*')
				->from('truck')
				->fetchAll();
		
		if( is_object($slotFrom) )
		{
			//metoda #1 - snazim se priradit vozidlo na pozadovany cas (dle $slotFrom)
			//echo '<h3>metoda 1</h3>';
			foreach ($trucks as $truck) 
			{
				//echo '<br>prochazim truck #'. $truck->id .'<br>';
				$orders = $this->connection->select('*')
					->from($this->getTable())
					->where('DATE(slotFrom) = %s', date('Y-m-d',$date->getTimestamp()) )
					->where('truck_id = %i', $truck->id)
					->orderBy('slotFrom ASC')
					->fetchAll();

				//pokud tento truck nema na dany den zakazku, rovnou ho priradim. Bud od slotFrom nebo od zacatku smeny.
				if( !$orders ) 
				{
					//echo 'nema zadne objednavky<br />';
					return Array('truck_id'=>$truck->id, 'slotFrom'=> (isset($slotFrom)) ? $slotFrom->getTimestamp() : $shiftBegin);
				}
				else 
				{
					//najdu posledni zakazku pred pozadovanym casem
					$beforeOrder = $this->connection->select('*')
						->from($this->getTable())
						->where('DATE(slotFrom) = %s', date('Y-m-d',$slotFrom->getTimestamp()) )
						->where('TIME(slotFrom) < %s', date('H:I:s',$slotFrom->getTimestamp()) )
						->where('truck_id = %i', $truck->id)
						->orderBy('slotFrom DESC')
						->fetch();
					//if( $beforeOrder ) echo "beforeOrder #". $beforeOrder->id .", from ". date('H:i',$beforeOrder->slotFrom->getTimestamp()) ."<br>";
					
					
					//najdu prvni zakazku po pozadovanem casu
					$afterOrder = $this->connection->select('*')
						->from($this->getTable())
						->where('DATE(slotFrom) = %s', date('Y-m-d',$slotFrom->getTimestamp()) )
						->where('TIME(slotFrom) > %s', date('H:I:s',$slotFrom->getTimestamp()) )
						->where('truck_id = %i', $truck->id)
						->orderBy('slotFrom ASC')
						->fetch();
					//if( $afterOrder ) echo "afterOrder #". $afterOrder->id .", from ". date('H:i',$afterOrder->slotFrom->getTimestamp()) ."<br>";
					
					
					//je pozadovany cas volny?
					if(
						$beforeOrder AND $afterOrder AND 
						$beforeOrder->slotFrom->getTimestamp() + $slotDuration <= $slotFrom->getTimestamp() AND 
						$afterOrder->slotFrom->getTimestamp() >= $slotFrom->getTimestamp() + $slotDuration
					)
					{
						return Array('truck_id'=>$truck->id, 'slotFrom'=>$slotFrom->getTimestamp());
					}
				}
			}
			reset($trucks);
		}
		
		
		//metoda #2 - snazim se priradit vozidlo na jiny nez pozadovany cas
		//echo '<h3>metoda 2</h3>';
		foreach ($trucks as $truck) 
		{
			//echo '<br>prochazim truck #'. $truck->id .'<br>';
			$orders = $this->connection->select('*')
				->from($this->getTable())
				->where('DATE(slotFrom) = %s', date('Y-m-d',$date->getTimestamp()) )
				->where('truck_id = %i', $truck->id)
				->orderBy('slotFrom ASC')
				->fetchAll();
			
			//pokud tento truck nema na dany den zakazku, rovnou ho priradim. Bud od slotFrom nebo od zacatku smeny.
			if( !$orders ) 
			{
				//echo 'nema zadne objednavky<br />';
				return Array('truck_id'=>$truck->id, 'slotFrom'=> (isset($slotFrom)) ? $slotFrom->getTimestamp() : $shiftBegin);
			}
			else 
			{
				$o=1;
				//prochazim zakazky dneho vozidla na dany den a snazim se pred nimi, mezi nimi nebo za nimi najit dostatecnou mezeru
				foreach ($orders as $order) 
				{
					//echo '<br>_prochazim order #'. $order->id .'<br>';
					//echo '_slotFrom = '. date('H:i',$order->slotFrom->getTimestamp()) .'<br>';
					//echo '_slotTo = '. date('H:i',$order->slotFrom->getTimestamp()+60*$order->slotDuration) .'<br>';
					
					if( $o==1 )
					{
						//je mezi zacatkem pracovni doby a zacatkem prvni zakazky cas?
						$remain = $order->slotFrom->getTimestamp() - $shiftBegin;
						if( $remain >= $slotDuration ) 
						{
							//echo '_na zacatku pracovni doby zbyva '. ($remain/60) .' minut<br>';
							return Array('truck_id'=>$truck->id, 'slotFrom'=>$shiftBegin);
						}
					}
					else
					{
						$gap = $order->slotFrom->getTimestamp() - $lastEnd;
						//echo '_mezera pred order #'. $order->id .' je '. ($gap/60) .' minut<br />';
						if( $gap >= $slotDuration ) return Array('truck_id'=>$truck->id, 'slotFrom'=>$lastEnd);
					}
					$lastEnd = $order->slotFrom->getTimestamp()+$order->slotDuration*60;
					$o++;
				}
				
				//zbyva do konce pracovni doby jeste cas?
				$remain = $shiftEnd - $lastEnd; // [s]
				if( $remain >= $slotDuration ) 
				{
					//echo '_do konce pracovni doby zbyva '. ($remain/60) .' minut<br>';
					return Array('truck_id'=>$truck->id, 'slotFrom'=>$lastEnd);
				}
				//else echo '_do konce pracovni doby zbyva JEN '. ($remain/60) .' minut<br>';
			}
			if( isset($lastEnd) ) unset($lastEnd);
		}
		return null;
	}
	
	
	public function getSummaryAssigned() 
	{
		return $this->connection->select('DATE(slotFrom) AS date, DAY(slotFrom) AS day, MONTH(slotFrom) AS month, YEAR(slotFrom) AS year, COUNT(*) AS counter')
			->from($this->getTable())
			->where( 'truck_id IS NOT NULL')
			->groupBy('DATE(slotFrom)')
			->orderBy('DATE(slotFrom) ASC')
			->fetchAll();
	}
	
	public function getSummaryNotAssigned() 
	{
		return $this->connection->select('DATE(pickupDate) AS date, DAY(pickupDate) AS day, MONTH(pickupDate) AS month, YEAR(pickupDate) AS year, COUNT(*) AS counter')
			->from($this->getTable())
			->where( 'slotFrom IS NULL OR truck_id IS NULL')
			->groupBy('DATE(pickupDate)')
			->orderBy('DATE(pickupDate) ASC')
			->fetchAll();
	}
	
	public function getDayList($year, $month, $day) 
	{
		$rows = $this->connection->select('*')
			->from($this->getTable())
			->where('( slotFrom IS NOT NULL AND YEAR(slotFrom) = %i AND MONTH(slotFrom) = %i AND DAY(slotFrom) = %i ) OR ( slotFrom IS NULL AND YEAR(pickupDate) = %i AND MONTH(pickupDate) = %i AND DAY(pickupDate) = %i )', $year, $month, $day, $year, $month, $day )
			->orderBy('TIME(slotFrom) ASC')
			->fetchAll();
		
		return ( $rows ) ? $this->createEntities($rows) : null;
	}
	
	public function findOrdersBy($filters, $offset=0, $perPage=100, $orderBy='id', $orderDir='ASC')
	{
		$select = $this->connection->select('`order`.id')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
				if( $key == 'truck' ) $select->where('truck_id = %i', $value );
			elseif( $key == 'shop' ) $select->where('pickupName = %s', $value );
			elseif( $key == 'date' ) 
			{
				preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($value), $matches);
				if( count($matches) == 4 ) 
				{
					$date = $matches[3] .'-'. $matches[2] .'-'. $matches[1];
					$select->where('(DATE(slotFrom) = %s OR (slotFrom IS NULL AND DATE(pickupDate) = %s))', $date, $date);
				}
			}
			
		}
		return $select->orderBy($orderBy.' '.$orderDir)->fetchAll($offset,$perPage);
	}
	
	public function countOrdersBy($filters)
	{
		$select = $this->connection->select('COUNT(`order`.id)')
				->from($this->getTable());
		foreach ($filters as $key => $value) 
		{
				if( $key == 'truck' ) $select->where('truck_id = %i', $value );
			elseif( $key == 'shop' ) $select->where('pickupName = %s', $value );
			elseif( $key == 'date' ) 
			{
				preg_match('/([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})/', trim($value), $matches);
				if( count($matches) == 4 ) 
				{
					$date = $matches[3] .'-'. $matches[2] .'-'. $matches[1];
					$select->where('(DATE(slotFrom) = %s OR (slotFrom IS NULL AND DATE(pickupDate) = %s))', $date, $date);
				}
			}
		}
		return $select->fetchSingle();
	}
	
	public function getShopPairs()
	{
		$pairs = Array();
		$rows = $this->connection->select('pickupName')
				->from($this->getTable())
				->fetchAll();
		
		foreach ($rows as $row) 
		{
			$pairs[$row->pickupName] = $row->pickupName;
		}
		return $pairs;
	}
}
?>
