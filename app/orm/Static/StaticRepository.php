<?php

namespace Model\Repository;

use Model\Repository\BaseRepository;
use \Nette\Caching\Cache;

/**
 * @table static
 */
class StaticRepository extends BaseRepository
{
	public $cache;

	public function injectCache(\Nette\Caching\Cache $cache)
    {
        $this->cache = $cache;
    }
	
	
	function findAll()
	{
		$content = $this->cache->load('static_all');
		if($content === NULL)
		{
			$content = new \stdClass();
			$rows = $this->connection->select('*')
					->from($this->getTable())
					->fetchAll();
			foreach ($rows as $row) 
			{
				if( $row->type == 'text' OR $row->type == 'template' OR $row->type == 'file' ) $content->{$row->code} = $row->text;
				elseif( $row->type == 'time' ) $content->{$row->code} = explode(':', $row->text);
				elseif( $row->type == 'timestamp' ) $content->{$row->code} = $row->timestamp;
			}
			$this->cache->save('static_all', $content, Array(
				Cache::TAGS => Array('static'), 
			));
		}
		return $content;
	}
	
	function loadAll()
	{
		$rows = $this->connection->select('*')
				->from($this->getTable())
				->fetchAll();
		return $this->createEntities($rows);
	}
	
	function findStatic($id)
	{
		$content = $this->cache->load('static_'. $id);
		if($content === NULL)
		{
			$row = $this->connection->select('*')
					->from($this->getTable())
					->where('id = %i', $id)
					->fetch();
			$content = $this->createEntity($row);
			$this->cache->save('static_'. $id, $content);
		}
		return $content;
	}
	
	//vrati jeden radek podle code
	function findByCode($code)
	{
		$content = $this->cache->load('static_'. $code);
		if($content === NULL)
		{
			$content = $this->connection->select('*')
					->from($this->getTable())
					->where('code = %s', $code)
					->fetch();
			$this->cache->save('static_'. $code, $content);
		}
		return $content;
	}
	
	//vrati vsechny radky jejihz code zacina na dany retezec
	function findByCodeLike($code)
	{
		return $this->connection->select('*')
			->from($this->getTable())
			->where('code LIKE %like~', $code)
			->fetchAll();
	}
}
?>
