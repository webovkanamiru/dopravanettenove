<?php

namespace Model\Entity;

use DateTime as PublishDate;
/**
 * @property int $id
 * 
 * @property string $code
 * @property string $name
 * @property string $type
 * @property string|null $text
 * @property PublishDate|null $timestamp
 */
class StaticEntity extends \LeanMapper\Entity
{
	
	const TYPE_TEXT			= 'text';
	const TYPE_TIMESTAMP	= 'timestamp';
	const TYPE_TEMPLATE		= 'template';
	const TYPE_FILE			= 'file';
	
	
	public static function getTypes() 
	{
		return Array(
			self::TYPE_TEXT			=> 'text',
			self::TYPE_TIMESTAMP	=> 'timestamp',
			self::TYPE_TEMPLATE		=> 'šablona',
			self::TYPE_FILE			=> 'soubor',
		);
	}
}
?>
