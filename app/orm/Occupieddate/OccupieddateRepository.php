<?php

namespace Model\Repository;

use Model\Repository\BaseRepository;

class OccupieddateRepository extends BaseRepository
{
	
	function getList()
	{
		$list = Array();
		$rows = $this->connection->select('*')
			->from($this->getTable())
			->fetchAll();
		foreach ($rows as $row) $list[] = date('Y-m-d',$row->date->getTimestamp());
		return $list;
	}
	
	
	function updateOneDay( $date, $workHoursBegin, $workHoursEnd, $slotDuration )
	{
		$data = Array();
		$shiftBegin = mktime($workHoursBegin[0], $workHoursBegin[1], 0, date('m',$date->getTimestamp()), date('d',$date->getTimestamp()), date('Y',$date->getTimestamp()));
		$shiftEnd   = mktime($workHoursEnd[0],   $workHoursEnd[1],   0, date('m',$date->getTimestamp()), date('d',$date->getTimestamp()), date('Y',$date->getTimestamp()));
		
		
		//nactu vsechny truck
		$trucks = $this->connection->select('id')
				->from('truck')
				->fetchAll();
		
		foreach ($trucks as $truck)
		{
			$data[ $truck->id ] = Array();
		}
		//print_r($data);
		$aOrders = $this->connection->select('id, truck_id, UNIX_TIMESTAMP(slotFrom) AS slotFrom')
			->from('order')
			->where('DATE(slotFrom) = %s', date('Y-m-d',$date->getTimestamp()) )
			->where('truck_id IS NOT NULL')
			->orderBy('truck_id, slotFrom ASC')
			->fetchAll();
		
		foreach ($aOrders as $order)
		{
			$data[$order->truck_id][] = $order;
		}
		//print_r($data);
		foreach ($data as $truck_id => $aOrders)
		{
			//nema zadne zakazky?
			if( count($aOrders) == 0 )
			{
				//echo "truck #$truck_id nema zadne zakazky<br>";
				return $this->deleteDay($date);
			}
			
			//je volno pred prvni zakazkou?
			if( $aOrders[0]->slotFrom - $shiftBegin >= $slotDuration*60 ) 
			{
				//echo "truck #$truck_id ma volno pred prvni zakazkou<br>";
				return $this->deleteDay($date);
			}
			
			//je volno mezi zakazkama?
			foreach ($aOrders as $i => $order) 
			{
				if( $i > 0 AND $order->slotFrom - $lastEnd >= $slotDuration*60 )
				{
					//echo "truck #$truck_id ma volno mezi zakazkama<br>";
					return $this->deleteDay($date);
				}
				$lastEnd = $order->slotFrom + $slotDuration*60;
			}
			
			//je volno po posledni zakazce?
			if( $shiftEnd - $aOrders[ count($aOrders)-1 ]->slotFrom - $slotDuration*60 >= $slotDuration*60 ) 
			{
				//echo "truck #$truck_id ma volno po posledni zakazce #". $aOrders[ count($aOrders)-1 ]->id ."<br>";
				return $this->deleteDay($date);
			}
			
		}
		return $this->insertDay($date);
	}
	
	
	function insertDay( \DateTime $date )
	{
		$exists = $this->connection->select('id')
			->from($this->getTable())
			->where('DATE(date) = %s', date('Y-m-d',$date->getTimestamp()) )
			->fetch();
		if( $exists ) return;
		return $this->connection->insert($this->getTable(), Array('date'=>$date) )
			->execute();
	}
	
	function deleteDay( \DateTime $date )
	{
		return $this->connection->query('DELETE FROM '. $this->getTable() .' WHERE DATE(date) = "'. date('Y-m-d',$date->getTimestamp()) .'"' );
	}
	
	
}
?>
