<?php

namespace Model\Entity;

/**
 * @property int $id
 * 
 * 
 * 
 * @property string $name
 * @property float $price
 */
class ServiceEntity extends \LeanMapper\Entity
{
	
	public function getNameprice() 
	{
		return ( $this->row->price > 0 ) ? $this->row->name .' ('. $this->row->price .' Kč)' : $this->row->name;
	} 
	
	
}
?>
