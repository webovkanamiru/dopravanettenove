<?php

namespace Model\Repository;

use Model\Repository\BaseRepository;

class ZipRepository extends BaseRepository
{
	
	function getList( $filters, $offset=0, $perPage=100, $orderBy='id', $orderDir='ASC' )
	{
		$select = $this->connection->select('*')
			->from('zip');
			
			foreach ($filters as $key => $value) 
			{
				//fulltext
				if($key=='q') 
				{
					$select->where('zip LIKE %~like~', $value);
				}
				
				elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
				elseif( is_null($value) ) $select->where($key .' IS NULL');
				elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
				else $select->where($key .' LIKE %like~', $value);
			}
		if( !is_null($orderBy) AND !is_null($orderDir) ) $select->orderBy($orderBy, $orderDir);
		
		if( !is_null($offset) AND !is_null($perPage) ) return $select->fetchAll($offset,$perPage);
		else return $select->fetchAll();
	}
	
	
	function countItemsBy( $filters )
	{
		$select = $this->connection->select('count(*)')
			->from('zip');
			foreach ($filters as $key => $value) 
			{
				//fulltext
				if($key=='q') 
				{
					$select->where('zip LIKE %~like~', $value);
				}
				
				elseif( is_numeric($value) ) $select->where($key .' = %i', $value);
				elseif( is_null($value) ) $select->where($key .' IS NULL');
				elseif( $value==='NOT-NULL' ) $select->where($key .' IS NOT NULL');
				else $select->where($key .' LIKE %like~', $value);
			}
		return $select->fetchSingle();
	}
	
}
?>
