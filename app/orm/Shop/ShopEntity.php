<?php

namespace Model\Entity;

/**
 * @property int $id
 * 
 * @property int $approved m:enum(self::APPROVED_*)
 * @property string $name
 * @property string|null $centre
 * @property string $street
 * @property string $house
 * @property int $zip
 * @property string $city
 * @property string $phone
 * @property string $email
 * @property string $contactPerson
 */
class ShopEntity extends \LeanMapper\Entity
{
	const APPROVED_YES  = 1;
	const APPROVED_NO = 0;
	
	
	
}
?>
