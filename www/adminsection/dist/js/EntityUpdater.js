function EntityUpdater(root)
{
	this.$root = $(root);
	
	this.entityName = this.$root.data('entityname');
	this.rowId		= this.$root.data('rowid');
	this.uploadUrl	= this.$root.data('uploadurl');
	
	//this.inputs = this.$root.find('input[type="text"]');
	
	//this.selects = this.$root.find('select');
	
	//this.textareas = this.$root.find('textarea');
	
	//radios, checkboxes, ... TODO
	
	if( this.rowId > 0 ) {}
	else throw "unable to initialize EntityUpdater - rowId missing";
	
	if( this.entityName.length > 0 ) {}
	else throw "unable to initialize EntityUpdater - entityName missing";
	
	if( this.uploadUrl.length > 0 ) {}
	else throw "unable to initialize EntityUpdater - uploadUrl missing";
	
	if( this.rowId > 0 && this.entityName.length > 0 && this.uploadUrl.length > 0 )
	{
		this.$root.on('focus', 'input[type="text"], textarea', $.proxy(this._inputFocusHandler, this));
		this.$root.on('blur',  'input[type="text"], textarea', $.proxy(this._inputBlurHandler, this));
		this.$root.on('change', 'select', $.proxy(this._inputBlurHandler, this));
	}
	
};	


EntityUpdater.prototype._inputFocusHandler = function(event) {
	$(event.target).removeClass('error').removeClass('success');
};

EntityUpdater.prototype._inputBlurHandler = function(event) {
	//console.log('_inputBlurHandler');
	var $input = $(event.target);
	var new_value = $input.val();

	this.disable(event.target);
	this.uploadChanges( $input.attr('name'), new_value );
};

EntityUpdater.prototype.getElement = function(rowId, columnName) {
	//console.log('getElement('+rowId+', '+columnName+')');
	return this.$root.find('[name="'+ columnName +'"]').first();
};

EntityUpdater.prototype.disable = function(element) {
	//console.log('disable()');
		 if( element.tagName == 'INPUT' )    $(element).val('..ukládám..').attr('disabled', true);
	else if( element.tagName == 'SELECT' )   $(element).attr('disabled', true);
	else if( element.tagName == 'TEXTAREA' ) $(element).html('..ukládám..').attr('disabled', true);
};
EntityUpdater.prototype.enable = function($element) {
	//console.log('enable()');
	$element.attr('disabled', false);
};
EntityUpdater.prototype.afterSuccessDelayed = function(rowId, columnName) {
	this.getElement(rowId, columnName).removeClass('success');
};

EntityUpdater.prototype.uploadChanges = function(columnName,newValue) {
	//console.log('uploadChanges()');
	this.request = $.ajax({
			type: 'GET',
			data: {entity_name:this.entityName, row_id:this.rowId, column_name:columnName, new_value:newValue},
			url: this.uploadUrl,
			context: this,
			success: this._uploadChangesSuccessHandler,
			error: this._uploadChangesErrorHandler
		});
};

EntityUpdater.prototype._uploadChangesSuccessHandler = function(response) {
	console.log('_uploadChangesSuccessHandler()');
	if( response.message == 'Error' ) return this._uploadChangesErrorHandler(response);
	var $el = this.getElement(response.row_id, response.column_name);
	
	$el.val(response.new_value);
	$el.addClass('success');
	
	window.setTimeout( $.proxy(this.afterSuccessDelayed, this), 2000, response.row_id, response.column_name);
	
	this.enable($el);
};

EntityUpdater.prototype._uploadChangesErrorHandler = function(response) {
	//console.log('_uploadChangesErrorHandler()');
	
	var $el = this.getElement(response.row_id, response.column_name);
	$el.val(response.new_value);
	$el.addClass('error');
	
	this.enable($el);
};
	
	

