/* Czech initialisation for the jQuery UI date picker plugin. */
/* Jan Drda <jdrda@outlook.com> */
(function( factory ) {
 if ( typeof define === "function" && define.amd ) {

  // AMD. Register as an anonymous module.

  define([ "../jquery.ui.datepicker" ], factory );
 } else {

  // Browser globals

  factory( jQuery.datepicker );
 }
}(function( datepicker ) {

  datepicker.regional['cs'] = {

  closeText: 'Zavřít',

  prevText: 'Předchozí',

  nextText: 'Další',

  currentText: 'Dnes',

  monthNames: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen',

  'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],

  monthNamesShort: ['led', 'úno', 'bře', 'dub', 'kvě', 'čer',

  'črc', 'srp', 'zář', 'říj', 'lis', 'pro'],

  dayNames: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],

  dayNamesShort: ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'],

  dayNamesMin: ['N','P','Ú','S','Č','P','S'],

  weekHeader: 'Týd.',

  dateFormat: 'dd.mm.yy',

  firstDay: 1,
  isRTL: false,

  showMonthAfterYear: false,

  yearSuffix: ''};
 datepicker.setDefaults(datepicker.regional['cs']);

 return datepicker.regional['cs'];

}));
